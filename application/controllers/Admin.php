<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('users_model','notices_model','society_master_model','users_model','document_collection_model','wallet_model','users_log_model','meetings_model'));

    }
    public function index()
	{
		$data = array();
		if($this->session->userdata('user_id'))
		{
			redirect(base_url().'admin/dashboard');
      
		}
		else
		{
		   $this->load->view('admin/login');
	  }
	}
	public function dashboard()
	{
	  $session_data = $this->session->userdata();
	  is_logged_in();
	  $role_id = $session_data['role_id'];
	  $user_id = $session_data['id'];
	  $society_id = $session_data['society_id'];
	  $data['notices_count'] = 0;
	  $data = array();
	  if($this->session->userdata('role_id') == SOCIETY_ADMIN || $this->session->userdata('role_id') == SOCIETY_MEMBER)
	  {
		  	$notices_count = $this->notices_model->getNoticesCount($user_id);
		    $data['notices_count'] = $notices_count[0]['count'];

		    $document_count = $this->document_collection_model->getDocumentsCount($user_id);
		    $data['document_count'] = $document_count[0]['count'];
		     $data['outstanding_amt']  = $this->wallet_model->where('user_id',$this->session->userdata('user_id'))->find_all();
		    $data['document_count'] = $document_count[0]['count'];

		    $get_meeting_data = $this->meetings_model->get_meeting_count();
		    $data['meeting_count'] = $get_meeting_data;

		    $get_event_count = $this->meetings_model->get_event_count();
		    $data['event_count'] = $get_event_count;
	  }
	  if($role_id == SOCIETY_SUPERUSER)
	  {
		  	$this->load->model('bill_payment_model');
		  	$bill_payment_count = $this->bill_payment_model->get_payment_count();
		  	if($bill_payment_count)
		  	{
		  		$data['bill_payment_count'] = $bill_payment_count[0]->id;
		  	}

		  	$this->load->model('document_collection_model');
		  	$document_count = $this->document_collection_model->getDocumentsread_Count();
		  	if($document_count)
		  	{
		  		 $data['read_count'] = $document_count[0]['read_count'];
		  	}
		  	$document_count = $this->document_collection_model->getDocumentsunread_Count();
		  	if($document_count)
		  	{
		  		 $data['unread_count'] = $document_count[0]['unread_count'];
		  	}
		  	$total_user_count = $this->users_model->column('count("id") as user_count')->where('society_id',$this->session->userdata('society_id'))->where('id !=',$this->session->userdata('user_id'))->where('status','Y')->find_all();
		  	if($total_user_count)
		  	{
		  		$data['user_count'] = $total_user_count[0]->user_count;
		  	}
		  	else
		  	{
		  		$data['user_count'] = 0;
		  	}
	  }
	  if($role_id ==SUPERADMIN)
      {
         $this->load->model('society_master_model');
         $society_count = $this->society_master_model->column('count(id) as total_count')->where('is_deleted','N')->find_all();
         if($society_count)
         {
           $data['society_count'] = $society_count[0]->total_count;
         }
         $data['map_data'] = $this->society_master_model->get_society_count();
      }
       if($role_id == SOCIETY_ADMIN)
      {
      	$total_user_count = $this->users_model->column('count("id") as user_count')->where('society_id',$this->session->userdata('society_id'))->where('id !=',$this->session->userdata('user_id'))->find_all();
		  	if($total_user_count)
		  	{
		  		$data['user_count'] = $total_user_count[0]->user_count;
		  	}
		  	else
		  	{
		  		$data['user_count'] = 0;
		  	}
      }

      if($role_id == SOCIETY_ADMIN ||$role_id ==SOCIETY_SUPERUSER)
      {
          $this->load->model('complaint_model');
          $where_array = array('is_solved' =>'N','society_id'=>$society_id);
         $res=$this->complaint_model->column('count("id") as count')->where($where_array)->find_all();
         // show($res,1);
         if($res)
         {
             $data['complaint_count'] = $res[0]->count;
         }
      }
      $data['selfVerify'] = $this->users_model->selfVerify();
	  load_back_view('admin/welcome',$data);
	}
	public function add_society()
	{
		load_back_view('add_society');
	}
	public function report()
	{
		load_back_view('report');
	}
	public function report_list()
	{
		$this->datatables->select('1,id,username');
        $this->datatables->from('users');
		$data = $this->datatables->generate();

		echo $data;
	}
	public function forgetPass() {

		$usr_name = $this->input->post('usr_name');

		$otp      = getRandomId(6);
		$data = array();

		if(!empty($usr_name))
		{
			$usr_exist = $this->users_model->where('username',$usr_name)->find_all();

			if($usr_exist) {

				$scid     = $usr_exist[0]->society_id;
				$id       = $usr_exist[0]->id;
				$email    = $usr_exist[0]->email;

				$dates = date('Y-m-d H:m:s');
				//OTP Inserted To
				$this->users_log_model->user_id=$id;
				$this->users_log_model->otp=$otp;
				$this->users_log_model->created_date=$dates;
				$this->users_log_model->updated_date=$dates;

				$user_log = $this->users_log_model->where('user_id',$id)->find_all();

				$inserted='';

				if($user_log) {
								$this->db->where('user_id',$id);
					$inserted = $this->db->update('users_log', array('otp' => $otp,'updated_date'=>$dates));
				}
				else
				{
					$inserted = $this->users_log_model->save();
				}

				if($inserted)
				{

					$society_details = $this->society_master_model->where('id',$scid)->find_all();
                    $society_name = $society_details[0]->name;
					$mail_replacement_array = array("{{society_name}}" => $society_name,
                                                    "{{useremail}}"    => $usr_exist[0]->first_name." ".$usr_exist[0]->last_name,
                                                      "{{otp}}"        => $otp,
                                                   );

                    $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),OTP_TEMPLATE);

					$params = array(
                                'to'        =>  $email,
                                'subject'   => 'OTP For Forget Password',
                                'html'      =>  $data['mail_content'],
                                'from'      =>  ADMIN_EMAIL,
                            );

                    $response = sendMail($params);
					$data['msg_type'] = 'success';
					$data['msg'] = 'OTP Sent to your Registerd Email Address';
				}
			}
			else
			{

				$data['msg_type'] = 'error';
				$data['msg'] = 'The entered username is not exist';
			}
		}
		else {
				$data['msg_type'] = 'error';
				$data['msg'] = 'Username Can not be empty';
		}
		echo json_encode($data);
	}
	public function changePass() {

		$input = $this->input->post();

		if($input)
		{

			$otp 		= $input['otp'];
			$pass 		= $input['password'];

			$otp 		= $this->users_log_model->where('otp',$otp)->find_all();

			if($otp)
			{
				$user_id   = $otp[0]->user_id;

				$salt  	   = getRandomId(8);
				$password  = md5($salt.$pass);
				$dt_arr    = array('salt' => $salt,'password'=>$password);

				$this->db->where('id',$user_id);
				$update_otp = $this->db->update('users',$dt_arr);

	            if ($update_otp)
	            {
	            	$this->db->delete('users_log',array('user_id'=>$user_id));

	            	$data['msg_type'] = 'success';
				    $data['msg'] = 'Password updated successfully';
	            }
	            else
	            {
	            	$data['msg_type'] = 'error';
				    $data['msg'] = 'failed to update please try again later';
	            }

			}
			else
			{

                $data['msg_type'] = 'error';
				$data['msg'] = 'Invalid Otp';
			}

		}
		else {
			$data['msg_type'] = 'error';
		    $data['msg'] = 'Invalid input...please provide valid input';
		}
		echo json_encode($data);
	}
}

?>
