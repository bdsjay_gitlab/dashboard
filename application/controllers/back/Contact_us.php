<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends MY_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        //$this->load->model(array('users_model','bill_payment_model','wallet_transaction_model','wallet_model','net_banking_model'));
        
    }
    public function index()
	{
		$data = array();
		load_back_view('admin/contact_us/contact_us',$data);
	}
	public function get_contact_list()
	{
		$data = $this->input->post();
		$this->datatables->select('1,id,DATE_FORMAT(created_at, "%d-%m-%Y"),full_name,email,subject');
        $this->datatables->add_column('action', '<a href='.base_url() . 'back/contact_us/get_contact_data/$1 title="View Contact Details" title="view notice" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon-th-list"></i> </a>', 'id');
          if (isset($data['from_date']) && $data['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(created_at, '%Y-%m-%d') >=", date('Y-m-d', strtotime($data['from_date'])));
        }

        if (isset($data['to_date']) && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(created_at, '%Y-%m-%d')<=", date('Y-m-d', strtotime($data['to_date'])));
        }
	    $this->datatables->from('contact_us');	
		$data = $this->datatables->generate();		

		
		echo $data;	
	}
	public function get_contact_data($id)
	{
        $this->db->where('id',$id);
        $this->db->from('contact_us');
        $result = $this->db->get()->result();
        $data['contact_data'] = $result[0];
        load_back_view('admin/contact_us/contact_us_view',$data);
 	}
}
?>
