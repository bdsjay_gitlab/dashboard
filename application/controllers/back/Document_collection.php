<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_collection extends CI_Controller {

		public function __construct() {
        parent::__construct();
        is_logged_in();
       $this->load->model('document_collection_model');
        }

	public function index()
	{
		$role_id = $this->session->userdata('role_id'); if($role_id == SUPERADMIN || $role_id == SOCIETY_SUPERUSER){
		$data=array();
		load_back_view('admin/document_collection/add_document',$data);
	}else
	{
		$data['documents']=$this->document_collection_model->all_documentUser();
		load_back_view('admin/document_collection/all_document',$data);
		
	}
	}

	// public function allDocumentUser()
	// {
	// 	$data['documents']=$this->document_collection_model->all_documentUser();
	// 	load_back_view('admin/document_collection/all_document_useriew',$data);
	// }

	public function addDocument()
	{
		$res=$this->document_collection_model->add_document();
		if($res)
		{
			$this->session->set_flashdata('msg','Add Document successfully');
          $this->session->set_flashdata('msg_type','success');
			$this->allDocument();
		}
	}
	public function allDocument()
	{
		
		$role_id = $this->session->userdata('role_id'); if($role_id == SUPERADMIN || $role_id == SOCIETY_SUPERUSER){
		$data['documents']=$this->document_collection_model->all_document();
		load_back_view('admin/document_collection/all_document',$data);
		}else
		{
			$data['documents']=$this->document_collection_model->all_documentUser();
			load_back_view('admin/document_collection/all_document',$data);
		}
	}
	public function deleteDocument($id)
	{
		$res=$this->document_collection_model->delete_document($id);
		if($res)
		{
			$this->session->set_flashdata('msg','Document Deleted successfully');
          $this->session->set_flashdata('msg_type','success');
			$this->allDocument();
		}
	}

	public function editDocument($id)
	{
		$data['editdata']=$this->document_collection_model->edit_document($id);
		load_back_view('admin/document_collection/edit_document',$data);
	}
	public function update_document()
	{
		$data=$this->document_collection_model->update_document();
		if($data)
		{
			$this->session->set_flashdata('msg','Document updated successfully');
          $this->session->set_flashdata('msg_type','success');
			$this->allDocument();
		}
	}
	public function viewDocument($id)
	{
		$data['editdata']=$this->document_collection_model->viewDocument_idwise($id);
		load_back_view('admin/document_collection/view_document',$data);
	}

	public function search_byUsername()
	{
		$name=$this->input->post('id');
		$res=$this->document_collection_model->search_username($name);
		echo json_encode($res);
	}
	public function readDocument($id)
	{
		$res=$this->document_collection_model->read_document($id);
		if($res)
		{
			$this->allDocument();
		}
	}
	
}