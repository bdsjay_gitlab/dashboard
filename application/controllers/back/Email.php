<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends MY_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model(array('Users_model','wallet_model','society_master_model'));
        
    }
    public function index($id=""){
       if($id)
       {
          $user_list = $this->Users_model->where('id',$id)->find_All();
          $wallet_details = $this->wallet_model->where('user_id',$user_list[0]->id)->find_All();
          $response['wallet_details'] = $wallet_details;
          $response['user_list']= $user_list;

          load_back_view('admin/email',$response);
       }
    }
    public function send_mail() {

       $input=$this->input->post();
       if($input)
       {
           $subject = $input['subject'];        
           $body = $input['mail'];        
           $send_to = $input['mail_to'];
           
           $user_id = $input['user_id'];
           $userdata = $this->users_model->where('id',$user_id)->find_All();
           
           $society_data = $this->society_master_model->where('id',$userdata[0]->society_id)->find_All();


           $mail_replacement_array = array("{{society_name}}" => $society_data[0]->name,
                                        "{{subject}}" => $subject,
                                        "{{body}}" => $body,
                                         "{{username}}" => $userdata[0]->first_name." ".$userdata[0]->last_name
                                       );
            
           $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),REMIANDER);

           $params = array(
                   'to'        =>  $send_to,
                   'subject'   =>  $subject,
                   'html'      =>  $data['mail_content'],
                   'from'      =>  ADMIN_EMAIL,
           );
           $response = sendMail($params);

           $this->session->set_flashdata('msg_type', 'success');
           $this->session->set_flashdata('msg','Email Send successfully');
       }   
       redirect(base_url().'back/bill_payment_report/outstanding_report');            
   }
    public function mail_to_all()
    {
       $society_id=$this->session->userdata('society_id');
       $response['user_list']=$this->Users_model->user_list($society_id);
       load_back_view('admin/email_to_all',$response);
    }
     public function send_mail_all() {
       $input=$this->input->post();
       $subject = $input['subject'];        
       $body = $input['mail'];        
       $send_to = $input['to'];
         
       if($input)
       {
           foreach ($send_to as $key => $value) {
               $userdata = $this->users_model->where('id',$value)->find_All();
               $email = $userdata[0]->email;
               
               $society_data = $this->society_master_model->where('id',$userdata[0]->society_id)->find_All();
               $mail_replacement_array = array("{{society_name}}" => $society_data[0]->name,
                                            "{{subject}}" => $subject,
                                            "{{body}}" => $body,
                                             "{{username}}" => $userdata[0]->first_name." ".$userdata[0]->last_name
                                           );
                
               $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),REMIANDER);

               $params = array(
                       'to'        =>  $email,
                       'subject'   =>  $subject,
                       'html'      =>  $data['mail_content'],
                       'from'      =>  ADMIN_EMAIL,
               );

              $response = sendMail($params);
           }
           $this->session->set_flashdata('msg', 'success');
           $this->session->set_flashdata('msg_type','Email Send successfully');
       }   
       redirect(base_url().'back/bill_payment_report/outstanding_report');            
   }
}
?>
