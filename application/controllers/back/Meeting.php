<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meeting extends MY_Controller {

		public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model('meetings_model');
        }
    public function meetingView() {
    	  $data['society'] = $this->selectSociety();
        $society_id=$this->session->userdata('society_id');

        $data['auth']=$this->meetings_model->get_auth($society_id);
        load_back_view('admin/meeting/meetingView',$data);
    }
    public function selectSociety() {
    	$res = $this->meetings_model->selectSociety();
    	return $res;
    }
    public function addMeeting() {

       $input = $this->input->post();
       $society_id=$input['society_id'];
       $authority=$input['authority'];
       $description=$input['description'];
       $subject=$input['subject'];
       $stime=$input['stime'];
       $etime=$input['etime'];

       $ondate = $input['ondate'];
       $ondate = date('Y-m-d',strtotime($ondate));
        $society_data=$this->db->where('id',$society_id)->get('society_master')->result();
       
        $society_name=$society_data[0]->name;
       if($input) 
       {    
            $meeting_id = getRandomId(5,'numeric');
            $res=$this->users_model->where_in('authority_id',$authority)->where('society_id',$society_id)->where('role_id !=',SOCIETY_SUPERUSER)->find_all();
            foreach ($res as $key => $value) 
            {
                $email=$value->email;    
                $phone=$value->phone;
                $username=$value->username;
                $first_name=$value->first_name;
                $last_name=$value->last_name;
                
                $mail_replacement_array = array("{{society_name}}" => $society_name,
                                                                "{{meeting_date}}" => $ondate,
                                                                "{{start_time}}" => $stime,
                                                                "{{end_time}}" =>  $etime,
                                                                "{{description}}" => $description,
                                                                "{{member}}"=>$first_name
                                                               );
                        
                $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),MEETING_SEND);

                $params = array(
                   'to'        =>  $email,
                   'subject'   =>  'Meeting Request',
                   'html'      =>  $data['mail_content'],
                   'from'      =>  ADMIN_EMAIL,
                 );
                $msg ="meeting request from: ".$society_name.", ".$subject.":Meeting Date: ".$ondate." meeting time : ".$stime." thank you"; 

                if(isset($input['both']))
                {
                      $response = sendMail($params);
                      $smsres = sendSms($phone,$msg);
                     
                }elseif(isset($input['email']))
                {
                      $response = sendMail($params);
                }else if(isset($input['sms'])){
                      $smsres = sendSms($phone,$msg);
                }else
                {

                }  
                $meeting_data = array(
                         'society_id'       => $society_id,
                         'meeting_date'     => $ondate,
                         'meeting_id'       => $meeting_id,
                         'user_id'          => $value->id,
                         'start_time'       => $input['stime'],
                         'end_time'         => $input['etime'],
                         'subject'          => $input['subject'],
                         'description'      => $input['description'],
                         'meeting_location' => $input['location']
               );
                $insert_meeting = $this->db->insert('meetings',$meeting_data);
            }

            if($insert_meeting)
            {
             $this->session->set_flashdata('msg', 'Meeting send successfully');
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Error Occured');
              $this->session->set_flashdata('msg_type', 'danger');   
            }
            
        }
            redirect(base_url().'back/meeting/allmeeting');

    }
    public function allMeeting() {

    	$data['meeting'] = $this->meetings_model->allMeeting();
    	load_back_view('admin/meeting/allMeeting',$data);
    }
    public function showMeeting($id) {

    	$meeting = $this->meetings_model->showMeeting($id);
        $meeting_id = $meeting[0]->meeting_id;

        $data['userdata'] = $this->db->select('u.id,u.first_name,u.last_name,ma.name')->from('users u')->join('meetings m','u.id = m.user_id','left')->join('member_authority ma','u.authority_id=ma.id','left')->where('m.meeting_id',$meeting_id)->get()->result();
    	$data['meeting'] = $meeting;
        load_back_view('admin/meeting/showMeeting',$data);
    }
    public function editMeeting($id) {

    	$data['society'] = $this->selectSociety();
    	$data['mt'] = $this->meetings_model->editMeeting($id);
    	load_back_view('admin/meeting/editMeeting',$data);	
    }
    public function updateMeeting() {

    	$input = $this->input->post();
        $ondate = $input['ondate'];
        $ondate = date('Y-m-d',strtotime($ondate));
    	if($input) {
    		$meeting_data = array(
    		 'society_id' 		=> $input['society_id'],
    		 'meeting_date' 	=> $ondate,
    		 'start_time' 		=> $input['stime'],
    		 'end_time' 		=> $input['etime'],
    		 'subject' 			=> $input['subject'],
    		 'description' 		=> $input['description'],
    		 'meeting_location'	=> $input['location']
    		);
    		$Mid = $input['Mid'];
    		$updated = $this->meetings_model->updateMeeting($Mid,$meeting_data);

              if($updated)
            {
             $this->session->set_flashdata('msg', 'Meeting Updated successfully');
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            }
    	}
        redirect(base_url().'back/meeting/allmeeting');
    }
    public function softDeleteMeeting($id) {
    	$deleted = $this->meetings_model->softDeleteMeeting($id);
        if($deleted)
            {
             $this->session->set_flashdata('msg', 'Meeting deleted successfully');
             $this->session->set_flashdata('msg_type', 'success');   
            }
            else
            {
              $this->session->set_flashdata('msg', 'Something Went Wrong');
              $this->session->set_flashdata('msg_type', 'danger');   
            }
            redirect(base_url().'back/meeting/allmeeting');
    }
}
?>