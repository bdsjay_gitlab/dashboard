<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userprofile extends MY_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        if(!$this->session->userdata('id'))
        {
        	redirect(base_url());
        }
        $this->load->model(array('users_model','Profile_model','society_master_model','house_master_model'));
        
    }

    public function index(){
		$session_data = $this->session->userdata();
		$id= $session_data['id'];
		$data['img'] = $this->Profile_model->img_select($id);
		$user_id = $session_data['id'];
		$profile = $this->Profile_model->super_profile($user_id);
		$fname = $profile[0]['first_name'];
		$lname = $profile[0]['last_name'];
		$email = $profile[0]['email'];
		$data['details'] = array($fname,$lname,$email);
		load_back_view('admin/my_profile/myprofile',$data);
	}

	public function myProfile_pic() {
		$session_data = $this->session->userdata();
		$id= $session_data['id'];
		$data = $this->input->post('image_name');
		$fname = $this->input->post('fname');
		$img['img'] = $this->Profile_model->img_select($id);
		$name = $img['img'][0]->profile_pic;
		$size = $_FILES['profile_pic']['size'];
		$ext = pathinfo($name,PATHINFO_EXTENSION);

		$allowed = array('png','jpg','jpeg','gif','');
		if(!in_array($ext,$allowed))
		{
			$this->session->set_flashdata('msg', 'Image With Extention .$ext not Allowed,Only png,jpg,jpeg or gif is allowed');
            $this->session->set_flashdata('msg_type', 'error');	
            redirect(base_url().'back/Userprofile'); 
		}
		else if($size>'2000000')
		{
			$this->session->set_flashdata('msg', 'Max Allowed Image Size is 2MB');
            $this->session->set_flashdata('msg_type', 'error');
            redirect(base_url().'back/Userprofile');
        }
		else{
			$image = $this->Profile_model->image_insert($id,$fname);
			$img['img'] = $this->Profile_model->img_select($id);
			$this->session->set_flashdata('msg', 'Image Upload Successfully');
	        $this->session->set_flashdata('msg_type', 'success');
	        redirect(base_url().'back/Userprofile');
		}
	}

	public function update_details(){
        
		$data = array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'email'=>$this->input->post('email'),
		);
		$update_data = $this->Profile_model->data_insert($data);
		if($update_data)
		{
				$this->session->set_flashdata('msg', 'Data Updated Successfully');
	            $this->session->set_flashdata('msg_type', 'success');
	    		redirect(base_url().'back/Userprofile');	
		}
	}

	public function match_pass(){
		$input=$this->input->post();

		$db_pass = $this->Profile_model->old_pass();
		$db_password=$db_pass[0]['password'];
		$salt=$db_pass[0]['salt'];

		$old_password=$input['old_password'];
		$old_password  =  md5($salt.$old_password);

	    $new_password  =  $input['new_password'];
	    $new_password  =  md5($salt.$new_password);

	    $confirm_password  =  $input['confirm_password'];
	    $confirm_password  =  md5($salt.$confirm_password);

	    if($old_password != '')
	    {
	    	if($old_password == $db_password)
	    	{
	    		if($old_password != $new_password)
	    		{
	    			if($new_password == $confirm_password)
	    			{
	    				$res=$this->Profile_model->set_newPassword($confirm_password);
	    				if($res)
	    				{
		    				$this->session->set_flashdata('msg', 'Password Updated Successfully');
		            		$this->session->set_flashdata('msg_type', 'success');
		    				redirect(base_url().'back/Userprofile');
	    				}
	    			}else
	    			{
		    			$this->session->set_flashdata('msg','New Password And Confirm Password Should Be Match');
		    			$this->session->set_flashdata('msg_type','error');
						redirect(base_url().'back/Userprofile');
	    			}
				}else
	    		{
		    		$this->session->set_flashdata('msg','We Cant Set New Password Same As Old Password');
		    		$this->session->set_flashdata('msg_type','error');
					redirect(base_url().'back/Userprofile');
	    		}
	    	}else
	    	{
	    	    $this->session->set_flashdata('msg','Your Enter Password Does Not match with your password');
	    		$this->session->set_flashdata('msg_type','error');
				redirect(base_url().'back/Userprofile');
	    	}
	    }else
	    {
	    	$this->session->set_flashdata('msg','Please Enter Old Password');
	    	$this->session->set_flashdata('msg_type','error');
			redirect(base_url().'back/Userprofile');
	    }	
	}
}

?>
