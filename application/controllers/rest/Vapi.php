<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vapi extends CI_Controller {

	public function __construct() {
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Allow-Headers: Content-Type');
		header('Content-Type: text/html; charset=utf-8');
		header('Content-Type: application/json; charset=utf-8');
		$this->load->model(array('Profile_model', 'users_model', 'house_master_model', 'complaint_model', 'meetings_model'));

		$data                 = array();
		$data['url']          = current_url();
		$data['Request_type'] = $_SERVER['REQUEST_METHOD'];
		$data['post']         = json_decode(file_get_contents('php://input'));
		$data['ip']           = $this->input->ip_address();
		log_data('Api/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $data, 'api');
	}

	public function sign_up() {
		$response = array('status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$role_id             = isset($input->role_id)?$input->role_id:'';
			$first_name          = isset($input->first_name)?$input->first_name:'';
			$last_name           = isset($input->last_name)?$input->last_name:'';
			$username            = isset($input->username)?$input->username:'';
			$email               = isset($input->email)?$input->email:'';
			$phone               = isset($input->phone)?$input->phone:'';
			$birth_date          = isset($input->birth_date)?$input->birth_date:'';
			$total_family_member = isset($input->total_family_member)?$input->total_family_member:'';
			$password            = isset($input->password)?$input->password:'';
			$salt                = getRandomId(8);
			$society_id          = isset($input->society_id)?$input->society_id:'';
			$house_id            = isset($input->house_id)?$input->house_id:'';
			$created_by          = isset($input->created_by)?$input->created_by:'';
			$created_date        = date("Y-m-d h:i:s");

			if (empty($society_id)) {
				$response['message'] = 'society_id is required.';
				$response['code']    = 201;
			} else if (empty($role_id)) {
				$response['message'] = 'Role Id is required.';
				$response['code']    = 201;
			} else if (empty($first_name)) {
				$response['message'] = 'First Name is required.';
				$response['code']    = 201;
			} else if (empty($last_name)) {
				$response['message'] = 'Last name is required.';
				$response['code']    = 201;
			} else if (empty($username)) {
				$response['message'] = 'Username is required.';
				$response['code']    = 201;
			} else if (empty($password)) {
				$response['message'] = 'Password is required.';
				$response['code']    = 201;
			} else if (empty($society_id)) {
				$response['message'] = 'Society Id is required.';
				$response['code']    = 201;
			} else if (empty($house_id)) {
				$response['message'] = 'House Id is required.';
				$response['code']    = 201;
			} else if (empty($email)) {
				$response['message'] = 'Email is required.';
				$response['code']    = 201;
			} else if (empty($phone)) {
				$response['message'] = 'phone number is required.';
				$response['code']    = 201;
			} else {

				$details = array(
					'first_name'          => $first_name,
					'last_name'           => $last_name,
					'username'            => $username,
					'email'               => $email,
					'phone'               => $phone,
					'birth_date'          => $birth_date,
					'total_family_member' => $total_family_member,
					'password'            => md5($salt.$password),
					'salt'                => $salt,
					'society_id'          => $society_id,
					'house_id'            => $house_id,
					'created_date'        => $created_date,
					'role_id'             => $role_id,
					'created_by'          => $created_by,
				);

				$res = $this->db->insert('users', $details);

				$response['code']    = 200;
				$response['status']  = true;
				$response['uid']     = $this->db->insert_id();
				$response['message'] = 'Registration Done !!';

				if ($res) {
					$this->db->set('allocated', 'Y');
					$this->db->where('id', $house_id);
					$this->db->update('house_master');
				} else {
					$response['message'] = 'Failed to register';
					$response['code']    = 203;
				}
			}

		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function change_password() {
		$response = array('code' => -1, 'status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input = json_decode(file_get_contents('php://input'));

			$old_pass = isset($input->old_pass)?$input->old_pass:'';
			$new_pass = isset($input->new_pass)?$input->new_pass:'';
			$id       = isset($input->userid)?$input->userid:'';

			if (empty($old_pass)) {
				$response['message'] = 'Old Password is required.';
				$response['code']    = 201;
			} else if (empty($new_pass)) {
				$response['message'] = 'New Password is required.';
				$response['code']    = 201;
			} else if (empty($id)) {
				$response['message'] = 'Userid is required.';
				$response['code']    = 201;
			} else {
				$userdata = $this->users_model->where('id', $id)->where('status', 'Y')->find_all();
				if ($userdata) {
					$salt         = $userdata[0]->salt;
					$userpassword = $userdata[0]->password;

					$old_password = md5($salt.$old_pass);

					if ($old_password == $userpassword) {
						$new_salt = getRandomId(8);
						$new_pass = md5($new_salt.$new_pass);

						$data = array(
							'salt'     => $new_salt,
							'password' => $new_pass,
						);

						$this->db->where('id', $id);

						$update_password = $this->db->update('users', $data);
						if ($update_password) {
							$response['message'] = 'Password Set Successfully';
							$response['status']  = true;
						} else {
							$response['message'] = 'Unable to set password';
							$response['status']  = false;
						}
					} else {
						$response['message'] = 'Password does not match with old password';
						$response['code']    = 201;
					}

				} else {
					$response['message'] = 'Inavalid user id';
					$response['code']    = 201;
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}
		echo json_encode($response);
	}

	public function edit_profile() {
		$response = array('code' => -1, 'status' => false, 'message' => '');
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$input      = json_decode(file_get_contents('php://input'));
			$id         = isset($input->id)?$input->id:'';
			$first_name = isset($input->first_name)?$input->first_name:'';
			$last_name  = isset($input->last_name)?$input->last_name:'';
			$email      = isset($input->email)?$input->email:'';

			if (empty($id)) {
				$response['message'] = 'User Id is required';
				$response['code']    = 201;
			} else if (empty($first_name)) {
				$response['message'] = 'Frist Name is required';
				$response['code']    = 201;
			} else if (empty($first_name)) {
				$response['message'] = 'Last Name is required';
				$response['code']    = 201;
			} else if (empty($first_name)) {
				$response['message'] = 'email is required';
				$response['code']    = 201;
			} else {
				$data = array(
					'first_name' => $first_name,
					'last_name'  => $last_name,
					'email'      => $email,
				);
				$update_data = $this->db->update('users', $data, array('id' => $id));
				if ($update_data) {
					$response['message'] = 'User Data Updated Successfully';
					$response['status']  = 200;
				} else {
					$response['message'] = 'Data Not Updated';
				}
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}

		echo json_encode($response);
	}

	public function insert_aj() {
		$response = array('message' => '', 'code' => -1, 'status' => false);
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			// $title=$this->input->post('title');
			// $start=$this->input->post('start');
			// $end=$this->input->post('end');
			$input = json_decode(file_get_contents('php://input'));
			$sid   = $input->society_id;
			if (empty($sid)) {
				$response['message'] = 'Title is required';
				$response['code']    = 201;
			} else {

				$res                 = $this->db->get_where('event_master', array('society_id' => $sid))->result_array();
				$response['code']    = 200;
				$response['status']  = true;
				$response['details'] = $res;
			}
		} else {
			$response['message'] = 'No direct script is allowed.';
			$response['code']    = 204;
		}

		echo json_encode($response);
	}

}

?>
