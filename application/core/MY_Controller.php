<?php

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();

		if ($this->config->item('maintenance_mode') == TRUE) {
			load_front_view(MAINTENANCE_VIEW);
		}
		$this->check_url_permission();
	}
	private function check_url_permission() {

		$first  = $this->uri->segment(1);
		$second = $this->uri->segment(2);
		$third  = $this->uri->segment(3);

		$url = $first.'/'.$second;

		$role_id   = $this->session->userdata('role_id');
		$role_name = $this->session->userdata('role_name');
		if ($url != '/') {
			if ($url != 'admin/dashboard') {
				if ($first == 'home') {
					return true;
				} else {
					if (!empty($role_id)) {

						$this->load->model(array('permission_model', 'menu_model', 'user_role_model'));

						$actionUrl = ($third != '')?$url.'/'.$third:$url;

						$menu = $this->menu_model->column('id, display_name,link')->where('link', $actionUrl)->as_array()->find_all();

						/*if ($menu == false) {
						$menu = $this->menu_model->column('id, display_name')->where('link', $url)->as_array()->find_all();
						}*/
						if ($menu == false) {
							return true;
						}

						$userRole    = $this->user_role_model->column('role_name,status')->where('id', $role_id)->as_array()->find_all();
						$permissions = $this->permission_model->column('action_id, action_name')->where('menu_id', $menu[0]['id'])->where('group_id', $role_id)->where('view', 'Y')->as_array()->find_all();
						if (empty($permissions)) {
							$base_url = base_url();
							echo "<center><b>You Dont Have Permissions To Access This Module</b><br><br><a href='".$base_url."admin/dashboard'><button type='button' class='btn' style='cursor:pointer'>Back</button></a>";
							exit;
						} else {

						}
					}
				}
			}
		}
	}
}// end of file
