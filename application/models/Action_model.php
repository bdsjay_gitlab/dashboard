<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Action_model extends MY_Model {

    var $table  = 'action';
    var $fields = array("id","name","label","relentity");
    var $key    = 'id';
   
    public function __construct() {
        parent::__construct();
       // $this->_init();      
    }

}
