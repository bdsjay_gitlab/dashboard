<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Amt_bifurcation_model extends MY_Model {

    protected $table = 'amt_bifurcation';
    var $fields = array("house_master_id", "Building_Repair","Lift_Repairs","Water_Charges","Property_Tax","Insurance_Charges","Service_Charges", "Other_Charges","updated_at","created_at");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

}
