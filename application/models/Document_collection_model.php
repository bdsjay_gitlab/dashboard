<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Document_collection_model extends MY_Model {

	protected $table = 'document_collection';
	var $fields      = array("id", "coming_from", "address_to", "society_id", "date", "handover_to");
	var $key         = 'id';

	public function __construct() {
		parent::__construct();
	}
	public function add_document() {
		$input = $this->input->post();
		$data  = array(
			'coming_from'   => $input['coming_from'],
			'address_to'    => $input['address_to'],
			'document_name' => $input['document_name'],
			'remark'        => $input['remark'],
			'date'          => $input['date'],
			'handover_to'   => $input['handover_to'],
			'society_id'    => $this->session->userdata('society_id'),
			'created_by'    => $this->session->userdata('id')
		);
		return $this->db->insert('document_collection', $data);
	}

	public function all_document() {
		return $this->db->order_by('id', 'desc')->get_where('document_collection', array('society_id' => $this->session->userdata('society_id')))->result();
	}

	public function delete_document($id) {
		return $this->db->delete('document_collection', array('id' => $id));
	}
	public function edit_document($id) {
		return $this->db->get_where('document_collection', array('id' => $id))->result_array();
	}
	public function update_document() {
		$input = $this->input->post();

		$data = array(
			'coming_from'   => $input['coming_from'],
			'address_to'    => $input['address_to'],
			'document_name' => $input['document_name'],
			'remark'        => $input['remark'],
			'date'          => $input['date'],
			'handover_to'   => $input['handover_to'],
			'society_id'    => $this->session->userdata('society_id'),
			'created_by'    => $this->session->userdata('id')
		);
		return $this->db->update('document_collection', $data, array('id' => $input['id']));
	}
	public function viewDocument_idwise($id) {
		return $this->db->get_where('document_collection', array('id' => $id))->result_array();
	}

	public function search_username($name) {
		$this->db->select('users.*,document_collection.*');
		$this->db->from('users');
		$this->db->join('document_collection', 'users.id = document_collection.address_to');
		$this->db->where('users.id', $name);
		$query = $this->db->get();
		return $query->result();
		//   return $this->db->get_where('document_collection',array('address_to'=>$name))->result_array();
		//  $sql="SELECT * from document_collection where address_to LIKE '%".$name."%'";
		// $query = $this->db->query($sql);
		//  return $query->result_array();
	}

	public function all_documentUser() {
		$query = $this->db->order_by('id', 'desc')->get_where('document_collection', array('address_to' => $this->session->userdata('id')));
		return $query->result();
	}
	public function read_document($id) {
		return $this->db->update('document_collection', array('document_view' => 'Y'), array('id' => $id));
	}

	public function getDocumentsCount($user_id) {
		$this->db->select('count(id) as count');
		$this->db->where('address_to', $user_id);
		$this->db->where('document_view', 'N');
		$this->db->from('document_collection');
		$result = $this->db->get()->result_array();
		return $result;
	}
	public function getDocumentsread_Count() {
		$this->db->select('count(id) as read_count');
		$this->db->where('society_id', $this->session->userdata('society_id'));
		$this->db->where('document_view', 'Y');
		$this->db->from('document_collection');
		$result = $this->db->get()->result_array();
		return $result;
	}
	public function getDocumentsunread_Count() {
		$this->db->select('count(id) as unread_count');
		$this->db->where('society_id', $this->session->userdata('society_id'));
		$this->db->where('document_view', 'N');
		$this->db->from('document_collection');
		$result = $this->db->get()->result_array();
		return $result;
	}

	public function get_document_id($user_id) {
		$this->db->select('id');
		$this->db->from('document_collection');
		$this->db->where('address_to', $user_id);
		$this->db->where('document_view', 'N');
		$result = $this->db->get()->result_array();
		return $result;
	}
}
