<?php
if(!defined('BASEPATH'))
exit('No Direct Access Script Allowed');

class Facility_master_model extends MY_Model{

	    var $table  = 'facility_master';
      var $fields = array("id","society_id","facility_name","adult_charges","child_charges","maintenance_cost","is_deleted","updated_by","updated_date"," created_by","created_at");
      var $key    = 'id';

    public function __construct() {
        parent::__construct();   
       // $this->_init();      
    }

  
}
?>