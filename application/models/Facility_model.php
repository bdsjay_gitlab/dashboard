<?php
if(!defined('BASEPATH'))
exit('No Direct Access Script Allowed');

class Facility_model extends MY_Model{

	    var $table  = 'facilty';
      var $fields = array("id","society_id","user_id","facilty_id","total_member","adult_count","facility_name","child_count","total_fees","updated_by","updated_at","created_at","created_by");
      var $key    = 'id';

    public function __construct() {
        parent::__construct();   
       // $this->_init();      
    }

    public function select_society(){
      $this->db->select('id,name');
      return $this->db->get('society_master')->result();
    }

    public function add_facility(){
      $adult_charges = $this->input->post('adult_charges');
      $child_charges = $this->input->post('child_charges');
      $data = array(
        'society_id' => $this->session->userdata('society_id'),
        'facility_name' => $this->input->post('facility_name'),
        'adult_charges' => $adult_charges ? $adult_charges : 0,
        'child_charges' => $child_charges ? $child_charges : 0,
        'is_deleted'=>'N'
      );
    	return $this->db->insert('facility_master',$data);
    }

    public function update_facility($id){
    	$this->db->where('id',$id);
    	return $this->db->get('facility_master')->result();
    }

    public function edit_facility(){

      $adult_charges = $this->input->post('adult_charges');
      $child_charges = $this->input->post('child_charges');
      $fac_id        = $this->input->post('fac_id');
      $data = array(
        'facility_name' => $this->input->post('facility_name'),
        'adult_charges' => isset($adult_charges) ? $adult_charges :'0',
        'child_charges' => isset($child_charges) ? $child_charges :'0'
      );
      return $this->db->update('facility_master',$data,array('id'=>$fac_id));
    }

    public function delete_facility($id){
      $this->db->set('is_deleted','Y');
      $this->db->where('id',$id);
      return $this->db->update('facility_master');
    }

    public function all_facility()
    {
      return $this->db->order_by('id','desc')->get_where('facility_master',array('society_id'=>$this->session->userdata('society_id'),'is_deleted'=>'N'))->result();
    }

    public function view_facility($id){
      return $this->db->get_where('facility_master',array('id'=>$id))->result();
    }

    //MEMBER FACILITY
    public function get_facility(){
      $this->db->select('id,facility_name');
      $this->db->where('is_deleted','N');
      $this->db->where('society_id',$this->session->userdata('society_id'));
      return $this->db->get('facility_master')->result();
    }
      
    public function get_users(){
      $this->db->select("u.id,u.role_id,CONCAT(u.first_name, ' ' , u.last_name) AS user_name,h.building,h.wing,h.block");
      $this->db->from('users u');
      $this->db->join('house_master h','h.id=u.house_id');
      $this->db->where('u.society_id',$this->session->userdata('society_id'));
      $this->db->where('status =','Y');
      $this->db->where('role_id !=',SUPERADMIN);
      $this->db->where('role_id !=',SOCIETY_SUPERUSER);
      return $this->db->get()->result();
    } 

   public function add_member(){


      $master=$this->db->get_where('facility_master',array('id'=>$this->input->post('facilty_id'),'society_id'=>$this->session->userdata('society_id')))->result();
      $adult_charges=$master[0]->adult_charges;
      $child_charges=$master[0]->child_charges;
      
      $adult_count = 0;
      if(!empty($this->input->post('adult_count')))
      {
         $adult_count = $this->input->post('adult_count');
      }

      $child_count = 0;
      if(!empty($this->input->post('child_count')))
      {
         $child_count = $this->input->post('child_count');
      }

      $total_adult_charges=($adult_count * $adult_charges);
       $total_child_charges=($child_count * $child_charges);
      $data = array(
        'society_id' => $this->session->userdata('society_id'),
        'facilty_id'=> $this->input->post('facilty_id'),
        'user_id'=> $this->input->post('user_id'),
        'adult_count'=> $adult_count,
        'child_count'=> $child_count,
        'total_member'=> $this->input->post('total_member'),
        'total_fees'=>  ($total_adult_charges+ $total_child_charges)
      );
      return $this->db->insert('facilty',$data);
    }

    public function all_member()
    {
      $this->db->select('concat(u.first_name," ",u.last_name) as username, u.id as uid,fm.facility_name,fm.id as fid,f.*');
      $this->db->from('facilty f');
      $this->db->join('facility_master fm','fm.id = f.facilty_id','left');
      $this->db->join('users u','u.id= f.user_id','INNER');
      $this->db->order_by('facilty_id','desc');
      $this->db->where('f.society_id',$this->session->userdata('society_id'));
      if($this->session->userdata('role_id') == SOCIETY_MEMBER)
      {
        $this->db->where('f.user_id',$this->session->userdata('id'));
      }
      $result = $this->db->get()->result();
      return $result;
    }

    public function view_userFacility($id)
    {
      return $this->db->get_where('facilty',array('id'=>$id))->result();
    }

    public function get_user_fac($id){
        $this->db->where('id',$id);
        return $this->db->get('facilty')->result();
    }

    public function dropdown($fid){
      $this->db->where('id',$fid);
      return $this->db->get('facility_master')->result();
    }

    public function user_dropdown($uid){
      $this->db->where('id',$uid);
      return $this->db->get('users')->result();
    }

    public function update_user_facility(){
      
      $master = $this->db->get_where('facility_master',array('id'=>$this->input->post('fid'),'society_id'=>$this->session->userdata('society_id')))->result();
      $adult_charges=$master[0]->adult_charges;
      $child_charges=$master[0]->child_charges;
      $total_adult_charges=($this->input->post('adult_count') * $adult_charges);
      $total_child_charges=($this->input->post('child_count') * $child_charges);
      $data = array(
        'society_id' => $this->session->userdata('society_id'),
        'facilty_id'=> $this->input->post('facilty_id'),
        'user_id'=> $this->input->post('user_id'),
        'adult_count'=> $this->input->post('adult_count'),
        'child_count'=> $this->input->post('child_count'),
        'total_member'=> $this->input->post('total_member'),
        'total_fees'=>  ($total_adult_charges+ $total_child_charges)
      );
      $this->db->where('id',$this->input->post('id'));
      return $this->db->update('facilty',$data);  
    }


    public function user_facilities(){
      return $this->db->get_where('facilty',array('society_id'=>$this->session->userdata('society_id'),'is_deleted'=>'N'))->result();
    }

    public function delete_user_fac($id){
      
      $this->db->where('id',$id);
      $this->db->delete('facilty');
      $this->db->set('is_deleted','Y');
      return $this->db->update('facilty');
    }
}
?>