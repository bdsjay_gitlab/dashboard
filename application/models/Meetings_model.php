<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Meetings_model extends MY_Model {

	protected $table = 'meetings';
	var $fields      = array("id", "society_id", "meeting_date", "start_time", "end_time", "subject", "description", "meeting_location", "status", "created_by", "created_date", "updated_by", "updated_date", "is_deleted");

	public function addMeeting($meeting_data) {

		$this->db->insert('meetings', $meeting_data);
		return true;
	}
	public function selectSociety() {
		$sess_uid  = $this->session->userdata('id');
		$sess_scid = $this->session->userdata('society_id');

		$this->db->select('s.id,s.name');
		$this->db->from('society_master s');
		if ($sess_uid == SUPERADMIN) {

		} else {
			$this->db->where('s.id', $sess_scid);
			$this->db->where('s.is_deleted', 'N');
		}
		$res = $this->db->get()->result();
		return $res;
	}
	public function allMeeting() {
		$sess_scid = $this->session->userdata('society_id');
		$sess_uid  = $this->session->userdata('id');
		$this->db->select('s.id,s.name,m.*');
		$this->db->from('society_master s');
		$this->db->join('meetings m', 's.id=m.society_id', 'right');
		$this->db->order_by('m.id', 'DESC');
		$this->db->group_by('meeting_id');
		$this->db->where('m.is_deleted', 'N');
		if ($sess_uid == SUPERADMIN) {

		} else {
			$this->db->where('s.id', $sess_scid);

		}
		if ($this->session->userdata('role_id') == SOCIETY_MEMBER) {
			$this->db->where('m.user_id', $sess_uid);
		} else if ($this->session->userdata('role_id') == SOCIETY_SUPERUSER) {
			$this->db->where('m.society_id', $sess_scid);
		}
		$res = $this->db->get()->result();
		return $res;
	}
	public function editMeeting($id) {
		$this->db->select('s.id,s.name,m.*');
		$this->db->from('society_master s');
		$this->db->join('meetings m', 's.id=m.society_id');
		$this->db->where('m.id', $id);
		$res = $this->db->get()->result();
		return $res;
	}
	public function showMeeting($id) {

		$this->db->select('s.id,s.name,m.*,u.first_name,u.last_name');
		$this->db->from('meetings m');
		$this->db->join('society_master s', 'm.society_id=s.id', 'left');
		$this->db->join('users u', 'm.created_by=u.id', 'left');
		$this->db->where('m.id', $id);
		$res = $this->db->get()->result();
		return $res;
	}
	public function updateMeeting($Mid, $meeting_data) {
		$this->db->where('id', $Mid);
		$this->db->update('meetings', $meeting_data);
		return true;
	}
	public function softDeleteMeeting($id) {
		$this->db->set('is_deleted', 'Y');
		$this->db->where('meeting_id', $id);
		$this->db->update('meetings');
		return true;

	}
	public function getdata($society_id, $table) {
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('society_id', $society_id);
		if ($table == 'meetings') {
			$this->db->where('is_deleted', 'Y');
			$this->db->order_by('created_date', 'desc');
		} elseif ($table == 'document_collection') {
			$this->db->order_by('date', 'desc');
		} else {
		}
		$data = $this->db->get()->result_array();
		if ($data) {
			return $data;
		}
		return false;
	}
	public function get_meeting_count() {
		$date        = date('d-m-Y H:i:s');
		$onlydate    = date('Y-m-d');
		$newDateTime = date('h:i A', strtotime($date));

		$this->db->select('count("id") as count');
		$this->db->from('meetings');
		$this->db->where('meeting_date >=', $onlydate);
		$this->db->where('society_id', $this->session->userdata('society_id'));
		$this->db->where('user_id', $this->session->userdata('user_id'));

		$result = $this->db->get()->result();

		return $result[0]->count;
	}
	public function get_event_count() {
		$date = date('d-m-Y');
		$this->db->select('count("id") as count');
		$this->db->from('event_master');
		$this->db->where("DATE_FORMAT(end, '%Y-%m-%d') >=", date('Y-m-d', strtotime($date)));
		$this->db->where('society_id', $this->session->userdata('society_id'));

		$result = $this->db->get()->result();
		return $result[0]->count;
	}
	public function get_auth($society_id) {
		$this->db->select('ma.id,ma.name');
		$this->db->from('member_authority ma');
		$this->db->join('users u', 'u.authority_id=ma.id', 'inner');
		$this->db->where('u.society_id', $society_id);
		$this->db->group_by('ma.id');
		$res = $this->db->get()->result();
		return $res;
	}

	public function get_meeting_id($time, $society_id) {
		$this->db->select('id');
		$this->db->from('meetings');
		$this->db->where('society_id', $society_id);
		$this->db->where('meeting_date', $time);
		$result = $this->db->get()->result_array();
		return $result;
	}
}