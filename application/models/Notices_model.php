<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Notices_model extends MY_Model {

	protected $table = 'notices';
	var $fields      = array("id", "notice_id", "notice_for", "society_id", "notice_from", "subject", "notice_to", "title", "notice_description", "notice_view", "created_by", "created_at");
	var $key         = 'id';

	public function __construct() {
		parent::__construct();
	}
	public function getNoticesCount($user_id) {
		$this->db->select('count(id) as count');
		$this->db->where('notice_to', $user_id);
		$this->db->where('notice_view', 'N');
		$this->db->from('notices');
		$result = $this->db->get()->result_array();
		return $result;
	}
	public function get_user_notices_count($notice_id) {
		$this->db->select('concat(u.first_name," ",u.last_name) as full_name,u.username,if(n.notice_view="Y","Seen","----") as is_view');
		$this->db->from('notices n');
		$this->db->join('users u', 'u.id = n.notice_to', 'left');
		$this->db->where('n.notice_id', $notice_id);
		$this->db->where('n.notice_to !=', SUPERADMIN_USER_ID);
		$this->db->where('n.notice_to !=', $this->session->userdata('id'));
		$result = $this->db->get()->result_array();
		return $result;
	}
	public function notices_seen($notice_id) {

		$this->db->select('u.id,concat(u.first_name," ",u.last_name) as full_name,if (n.notice_view="Y","seen","unseen") as view_status');
		$this->db->from('notices n');
		$this->db->join('users u', 'u.id = n.notice_to', 'left');
		$this->db->where('n.notice_id', $notice_id);
		$res = $this->db->get()->result();
		if ($res) {
			return $res;
		} else {
			return false;
		}

	}

	public function get_notice_id($user_id) {
		$this->db->select('id');
		$this->db->from('notices');
		$this->db->where('notice_to', $user_id);
		$this->db->where('notice_view', 'N');
		$result = $this->db->get()->result_array();
		return $result;
	}

}
