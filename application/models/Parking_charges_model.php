<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parking_charges_model extends MY_Model {

    protected $table = 'parking_charges';
    var $fields = array("id", "society_id","vehicle_type","parking_amt","lease_amt","updated_at","created_at");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }
    public function delete_park($id){
    	$this->db->where('id',$id);
    	$this->db->delete('parking_charges');
    	return true;
    }

}
