<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Permission_model extends MY_Model {

      var $table  = 'permission';
    var $fields = array("id","group_id","menu_id","action_id","action_name","view","add","edit","delete","record_status","created_by","created_date");
    var $key    = 'id';

    public function __construct() {
        parent::__construct();   
       // $this->_init();      
    }
    
    function get_data(){
        $this->db->select('g.name,m.id,m.display_name,p.group_id,p.menu_id,p.action_id,p.view,p.add,p.edit,p.delete,p.record_status');
        $this->db->from('permission p');
        $this->db->join('menu m','m.id=p.menu_id','left');
        $this->db->join('groups g','m.id=p.group_id','left');
        $this->db->where('m.parent','0');
        $menu_result = $this->db->get();
        $menu_array = $menu_result->result_array();
      //  show($this->db->last_query(),1);
        return $menu_array;
    }
    public function get_data_group_wise($group_id){
        $this->db->select('g.name,m.id,m.display_name,p.group_id,p.menu_id,p.action_id,p.view,p.add,p.edit,p.delete,p.record_status');
        $this->db->from('permission p');
        $this->db->join('menu m','m.id=p.menu_id','left');
        $this->db->join('groups g','m.id=p.group_id','left');
        $this->db->where('m.parent','0');
        $this->db->where('p.group_id',$group_id);
        $menu_result = $this->db->get();
        $menu_array = $menu_result->result_array();
        
        return $menu_array;
    }
    public function sub_menu_data($group_id){
       $query_sel="SELECT m.id, m.display_name,m.parent,p.group_id,p.menu_id,p.action_id,"
                . "if(p.view IS NOT NULL ,p.view,'N') as vview,"
                . "if(p.add IS NOT NULL,p.add,'N' ) aadd,"
                . "if(p.edit IS NOT NULL ,p.edit,'N') eedit,if(p.delete IS NOT NULL,p.delete,'N') ddelete,"
                . "if(p.record_status IS NOT NULL ,p.record_status,'N') per_status,m.record_status"
                . " FROM `menu` m left join permission p on m.id=p.menu_id  and p.record_status='Y' and p.group_id = ".$group_id."   "
                . "where m.parent !=0 and m.record_status='Y' order by m.id ";
         $result = $this->db->query($query_sel);
    
        $data=$result->result_array(); 
      
        return $data;
    }
     public function parent_menu_data($group_id){
       $query_sel="SELECT m.id, m.display_name,m.parent,p.group_id,p.menu_id,p.action_id,"
                . "if(p.view IS NOT NULL ,p.view,'N') as vview,"
                . "if(p.add IS NOT NULL,p.add,'N' ) aadd,"
                . "if(p.edit IS NOT NULL ,p.edit,'N') eedit,if(p.delete IS NOT NULL,p.delete,'N') ddelete,"
                . "if(p.record_status IS NOT NULL ,p.record_status,'N') per_status,m.record_status"
                . " FROM `menu` m left join permission p on m.id=p.menu_id  and p.record_status='Y'  and p.group_id = ".$group_id."   "
                . "where m.parent =0 and m.record_status='Y' order by m.id  ";
        $result = $this->db->query($query_sel);
   
        $data=$result->result_array(); 
      
        return $data;
    }
    
    function sub_sub_menu($group_id){
        
        $query_sel="SELECT m.id, m.display_name,m.parent,p.group_id,p.menu_id,p.action_id,if(p.view IS NOT NULL ,p.view,'N') as vview,"
                . "if(p.add IS NOT NULL,p.add,'N' ) aadd,if(p.edit IS NOT NULL ,p.edit,'N') eedit,"
                . "if(p.delete IS NOT NULL,p.delete,'N') ddelete,if(p.record_status IS NOT NULL ,p.record_status,'N') per_status,"
                . "m.record_status FROM `menu` m left join permission p on m.id=p.menu_id  and p.record_status='Y' and p.group_id = 1 "
                . "where m.parent !=0 and m.record_status='Y' order by m.id  ";
        $result = $this->db->query($query_sel);
   
        $data=$result->result_array(); 
     
        return $data;
    }
    public function get_menus_details($id,$url)
    {
        show($id,1);
    }

}
