<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Society_house_charges_model extends MY_Model {

    protected $table = 'society_house_charges';
    var $fields = array("id", "society_id","house_type","maintenance_charges","lease_amt","created_by","updated_at","created_at");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }
 	public function editHouseType($id,$ses_scid) {

        	$this->db->select('shc.house_type,shc.id as hid');
            $this->db->from('society_house_charges shc');
            $this->db->where('shc.society_id',$ses_scid);
     $res = $this->db->get()->result();
        
         // $res = $this->db->query("SELECT house_type,id as hid FROM society_house_charges where id not in (select house_type from house_master where id=$id) and society_id=$ses_scid")->result();
         // show($this->db->last_query(),1);
	return $res;
	}
    public function delete_data($id){
        $this->db->where('id',$id);
        $this->db->delete('society_house_charges');
        return true;
    }

}
