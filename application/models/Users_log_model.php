<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class users_log_model extends MY_Model {

    protected $table = 'users_log';
    var $fields = array("id", "user_id","otp","created_date","updated_date");
}
