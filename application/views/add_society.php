<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                     <div class="form-group">
                                <label class="col-lg-3 control-label" for="provider name"></label>
                                <div class="col-lg-6">
                                    <h2>Add Society</h2>
                                </div>
                            </div>                             
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <form action="http://180.92.175.107/bos_testing/index.php/master/discount/store"  id="prov_create_form" method="post" name="prov_create_form">
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="provider name">Society Name:</label>
                                <div class="col-lg-6">
                                    <input name="disc_name" type="text" id="disc_name" value="" class="form-control"   >
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="form-group">

                                <label class="col-lg-3 control-label" for="provider name">Discount Description:</label>
                                <div class="col-lg-6">
                                    <input name="disc_desc" type="text" id="disc_desc" value="" class="form-control"   >
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                           
                           <div class="form-group">
                                <label class="col-lg-3 control-label" for="commission from">From Date</label>
                                <div class="col-lg-6">
                                    <input class="form-control date" id="from"  name="from" placeholder="" value="" type="text" >
                                    
                                    <span id="from_msg" class="err"></span>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="commission from">Till Date</label>
                                <div class="col-lg-6">
                                    <input class="form-control date" name="till" id="till"value="" type="text" >
                                    
                                    <span id="from_msg" class="err"></span>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="op_addr_1">Type Of Discount: </label>
                                <div class="col-lg-6">
    <!--                                <input name="prov_type" type="text" maxlength="40" id="prov_type" class="form-control"   >-->
                                    <select class="form-control" id="type_of_disc"  name="type_of_disc" value=""  >
                                        <option value="flat"  >Flat</option>
                                        <option value="percentage"  >Percentage(%)</option>
                                        
                                    </select>
                                    <span id="prov_type_msg" class="err"></span>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div> <div class="form-group">
                                <label class="col-lg-3 control-label" for="typeOfUser">Type Of User: </label>
                                <div class="col-lg-6">
    
                                    <select class="form-control" id="type_of_user"  name="type_of_user">
                                        <option value="7" selected>Customer</option>
                                        <option value="16">Agent</option>
                                        <option value="22">Mobile APP User</option>
                                        <option value="7,16,22">ALL</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div><div class="form-group">
                                <label class="col-lg-3 control-label" for="discount_on">Discount On: </label>
                                <div class="col-lg-6">
    
                                    <select class="form-control" id="discount_on"  name="discount_on">
                                        <!-- <option value="base_fare" >Base Fare</option> -->
                                        <option value="seat_fare" selected>Seat Fare</option>
                                        <!-- <option value="total_fare">Total Fare</option> -->
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="op_addr_1">Value   : </label>
                                <div class="col-lg-6">
                                    <input name="value_disc" type="text" value="" id="value_disc" class="form-control"   >
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            
                    
                                <div class="form-group">
                                    <div class="col-lg-offset-4">
                                        <button class="btn btn-primary" id="save_prov_data" name="save_prov_data" type="submit">Save</button> 
                                        <a class="btn btn-primary" href="http://180.92.175.107/bos_testing/master/discount" type="button">Back</a> 
                                    </div>
                                </div>

                        </form>
                    </div><!-- /.box-body -->
                <!--</div> /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
