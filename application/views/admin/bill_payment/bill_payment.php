<section class="content">
    <div id="mail_template_wrapper">
        <?php
        $this->load->view(BILL_TEMPLATE_AJAX);
        ?>
    </div>

    <?php $this->load->view(BILL_TEMPLATE_POPUP); ?>
</section>
<!-- CK Editor -->
<script type="text/javascript">
$(document).ready(function() {
    var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "back/bill_payment/get_payment_data",
            "type": "POST",
            "data": "json",
            data   : function (d) {
                        d.from_date    =  $("#from_date").val();
                        d.to_date    =  $("#till_date").val();
                        d.transaction_type    =  $("#trans_type").val();
                     }
        },
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        //        "paginate": true,
        "paging": true, 
        "searching": true,
        "order": [[1, "desc"]],
        "aoColumnDefs": [
                    {
                        "targets": [1,2],
                        "visible": false,
                        "searchable": false

                    }
                ],

         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();  
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);  
             if (aData[8] == 'success' || aData[8] == 'rejected' || aData[2] == '<?php echo $this->session->userdata('role_id')?>')
            {
                $("td:eq(8)", nRow).html('<a href='+base_url+'back/bill_payment/bill_payment_view/'+aData[1]+' class="views_wallet_trans margin"><i class="glyphicon glyphicon-eye-open"></i></a>');
            }          
            return nRow;
        },
    };

    var oTable = $('#mail_display_table').dataTable(tconfig); 
    $(document).off('click', '#generate_btn').on('click', '#generate_btn', function (e) { 
            from_date    =  $("#from_date").val();
            to_date    =  $("#till_date").val();
            
            if(from_date === '' && to_date)
            {
                alert("please provide from date");
            }
            else if(from_date && to_date === '')
            {
                alert("please provide Till date");
            }
            else{
                oTable.fnDraw(); 
            }    
    }); 
});
</script>