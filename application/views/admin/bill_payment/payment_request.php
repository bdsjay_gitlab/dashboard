<?php 
$res=$this->db->where('user_id',$this->session->userdata('user_id'))->get('wallet')->result(); 
?>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                            <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Payment Request
                                     <hr>
                                </h3>
                            </div>
                            <div class="col-lg-3"> </div>
                            <div class="col-lg-2"> 
                              <h3 style="text-align: center;">
                                           <button type="button" class="form-control btn btn-info" data-toggle="modal" data-target="#myModal">Net-Banking Details</button> 
                                           <hr>
                                </h3>
                                </div>
                        </div>
                      <div class="box-body">
                         
                          <form id="payment_request" name="payment_request" method="post" action="<?php echo  base_url().'back/bill_payment/save_payment'?>">
                            <div class="form-group">
                                    <label class="col-lg-2 control-label">Transaction Type</label>
                                        <div class="col-lg-4"> 
                                         <select class="status form-control" name="transaction_type" id="transaction_type" required="">
                                                <option value="">Select Transaction</option>
                                                <option value="cash">Cash</option>
                                                <option value="cheque">Cheque</option>
                                                <option value="netbanking">Netbanking</option>
                                        </select>
                                        </div>
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                               <div class="form-group">
                                     <label class="col-lg-2 control-label">Amount </label>
                                     <div class="col-lg-4">
                                         <input type="text" class="form-control" name="amount" id="amount" value="<?php if($res){ echo $res[0]->current_outstanding;} ?>">
                                    </div>
                               </div>
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group bank_name">
                                    <label class="col-lg-2 control-label" for="bank name">Bank Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="bank_name" id="bank_name" value="">
                                    </div>
                                </div>
                              <div class="clearfix bank_name" style="height: 10px;clear: both;"></div>
                              <div class="form-group acc_no">
                                    <label class="col-lg-2 control-label" for="bank name">Bank Acc no</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="acc_no" id="acc_no" value="">
                                    </div>
                                </div>
                            <div class="clearfix branch_name" style="height: 10px;clear: both;"></div>
                            <div class="form-group branch_name">
                                    <label class="col-lg-2 control-label" for="bank name">Branch Name</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="branch_name" id="branch_name" value="">
                                    </div>
                                </div>
                            <div class="clearfix branch_name" style="height: 10px;clear: both;"></div>
                            <div class="form-group ifsc">
                                    <label class="col-lg-2 control-label" for="bank name">Ifsc code</label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="ifsc" id="ifsc" value="">
                                    </div>
                                </div>
                            <div class="clearfix ifsc" style="height: 10px;clear: both;"></div>
                                <div class="form-group trans_ref">
                                    <label class="col-lg-2 control-label" for="trans_ref">Transaction Ref No</label>
                                    <div class="col-lg-4">
                                       <input type="text" class="form-control" name="trans_ref" id="trans_ref" value="">
                                    </div>
                                </div>

                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                                 <div class="form-group check_no">
                                    <label class="col-lg-2 control-label" for="trans_ref">Check No</label>
                                    <div class="col-lg-4">
                                       <input type="text" class="form-control" name="check_no" id="trans_ref" value="">
                                    </div>
                                   </div>
                                     <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Payment Date </label>
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="transaction_date" id="transaction_date" value="">
                                    </div>
                                </div>
                                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                                   <div class="form-group"  id="reason">
                                         <label class="col-lg-2 control-label"> Remark: </label>
                                         <div class="col-lg-4">
                                               <textarea id="comment" class="form-control" style="height: 120px;border: 1px outset" placeholder="Write Your Comment Here....!!" name="comment" row="3"></textarea>
                                         </div>
                                   </div>  
                              <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                        <div class="col-lg-offset-3">
                                            <button class="btn btn-primary" type="submit">Save</button> 
                                            <a class="btn btn-primary" href="<?php echo base_url().'back/bill_payment'?>" type="button">Back</a> 
                                        </div>
                                </div>
                          </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
         <form id="modal_form" name="modal_form">           
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><center><b>NET-BANKING DETAILS</b></center></h4>
          </div>
        <div class="modal-body">
            <div class="form-group row">
                   <div class="col-md-12">
                         <table class="table">
                            <thead>
                              <tr>
                                <th>Sr.No</th>
                                <th>A/c Name</th>
                                <th>A/c No</th>
                                <th>Bank Name</th>
                                <th>IFSC Code</th>
                                <th>Branch & City</th>
                              </tr>
                            </thead>
                            <tbody>
                               <?php if(isset($netbanking_details) || !empty($netbanking_details)){?>
                                 <?php $c=0; foreach($netbanking_details as $key => $value){  $c++; ?>
                                     <tr>
                                      <td><?php echo $c;?></td>
                                      <td><?php echo $value['acc_name']?></td>
                                      <td><?php echo $value['acc_no']?></td>
                                      <td><?php echo $value['bank_name']?></td>
                                      <td><?php echo $value['ifsc_code']?></td>
                                      <td><?php echo $value['branch']?></td>
                                 </tr> 
                                 <?php }?>
                              <?php }?>
                             </tbody>
                          </table>
                    </div>
          </div>
        </div>
         
        <div class="modal-footer">
            <button type="button" class="btn btn-default btn-danger" name="cancel" id="cancel" data-dismiss="modal">Close &times;</button>
        </div>
           </div>
        </div>
      </div> 
   </form>
<script>
  $( function() {
       $( "#transaction_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
</script>
<script type="text/javascript">
    $(document).ready(function () {

    $("#payment_request").validate({
        rules: {
            amount: {
                required: true,
                number: true
            },
            bank_name: {
                required: true,
                lettersonly: true
            },
            acc_no: {
                required: true,
                number:true
            },
            branch_name: {
                required: true,
                alphanumeric:true
            },
            check_no : {
              required : true,
              alphanumeric:true
            },
            trans_ref: {
                required: true,
                alphanumeric:true
            },
            transaction_date: {
                required: true,
            },
            ifsc:{
                required:true,
                lfsccode:true
            }
        },
        errorPlacement: function (error, element) {
                   
            if(element.attr("name") === "transaction_type") {
                error.appendTo(element.parent("div") );
            } else if(element.attr("name") === "agent_city") {
                error.appendTo(element.parent("div") );
            } else {
                error.insertAfter(element);
            }
                
        },
        messages: {
                    
            amount: {
                required: "Please Enter amount",
                number: "Please Enter Valid amount",
            },
            bank_name: {
                required: "Please Enter bank name",
            },
            acc_no: {
                required: "Please Enter Account number",
                number : "only numbers are allowed"
            },
            branch_name: {
                required: "Please Enter branch number",
            },
            trans_ref: {
                required: "Please Enter trans_ref"
            },
            transaction_date: {
                required: "Please Select The Transaction Date",
            },
            check_no : {
              required : "please enter cheque no"
            },
            ifsc:{
                required: "Please enter ifsc code",
            }
        }
    });

    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
    }, "please provide letters only");
    
     jQuery.validator.addMethod("lfsccode", function(value, element) {
        return this.optional(element) || /^[a-z]{4}\d{7}$/i.test(value);
    }, "Please provide valid ifsc code");

    

});

</script>
<script type="text/javascript">
     $(document).off('change','#transaction_type').on('change','#transaction_type', function() {
      
      if ( this.value == 'cash')
      {
        $(".bank_name").hide();
        $(".trans_ref").hide();
        $(".acc_no").hide();
        $(".ifsc").hide();
        $(".branch_name").hide();
        $(".check_no").hide();

      }
    else if ( this.value == 'cheque')
      {
        $(".bank_name").show();
        $(".trans_ref").hide();
        $(".acc_no").show();
        $(".ifsc").hide();
        $(".branch_name").hide();
        $(".check_no").show();    
        
      }
      else if(this.value == 'netbanking')
      {
        $(".bank_name").show();
        $(".trans_ref").show();
        $(".acc_no").show();
        $(".ifsc").show();
        $(".branch_name").show();
        $(".check_no").hide();   
      }
    });
</script>