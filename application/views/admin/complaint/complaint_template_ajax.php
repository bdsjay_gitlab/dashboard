<div class="row">
    <h3 style="padding-left: 15px;">Complaint List</h3>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <?php if($this->session->userdata('role_id')==SOCIETY_MEMBER || $this->session->userdata('role_id')==SOCIETY_ADMIN){?>
                <span style="float: right;margin-top: 10px;margin-right: 10px;"><button class="btn btn-success add_btn">File a Complaint</button></span>

                <?php } ?>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="mail_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="40px">Sr No</th>
                            <th>Id</th>
                            <th>Complaint From</th>
                            <th>Date and time</th>
                            <th>Complaint Subject</th>
                            <th>Acknowledge</th>
                            <th width="100px">View Complaint</th>
                        </tr>
                    </thead>
                    <tbody>                                           

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>



