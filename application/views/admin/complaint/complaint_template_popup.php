<div class="modal fade popup" id="add_edit_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php
            $attributes = array("method" => "POST", "id" => "complaint_form", "name" => "complaint_form");
            echo form_open(base_url().'back/complaint/add_complaint', $attributes);
            ?>
            <div class="modal-body">
                 <div class="form-group">
                    <label for="title">Complaint Subject:</label>
                    <input type="text" placeholder="Complaint Subject" name="complaint_subject" id="title" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="class">Complaint Discription:</label>
                    <textarea placeholder="Write Your Complaint Here" id="mail_body" name="complaint_discription"  class="form-control"></textarea>

                    <input name="id" type="hidden" id="id" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Cancel</button>

                <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Save</button>
            </div>
        </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- <script type="text/javascript">
    $(function() {
        
        CKEDITOR.replace('mail_body_class',{
            enterMode : CKEDITOR.ENTER_BR,
            entities : false,
            basicEntities : false,
            entities_greek : false,
            entities_latin : false, 
            htmlDecodeOutput : false
        });
        
    });
</script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#complaint_form').validate({
    rules:{
        complaint_discription:{
            required:true
        },
        complaint_subject:{
            required:true
           
        }
    },
    messages:{
        complaint_discription:
        {
            required:"Complaint Description is required",
        },
        complaint_subject:{
            required:"Complaint subject is required",
           
        }
    }
  });
}); 
</script>
