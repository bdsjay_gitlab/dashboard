<section class="content">
    <div id="mail_template_wrapper">
        <?php
        $this->load->view('admin/contact_us/contact_us_ajax');
        ?>
    </div>
</section>
<!-- CK Editor -->
<script type="text/javascript">
$(document).ready(function() {
    var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "back/contact_us/get_contact_list",
            "type": "POST",
            "data": "json",
            data   : function (d) {
                        d.from_date    =  $("#from_date").val();
                        d.to_date    =  $("#till_date").val();
                     }
        },
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        //        "paginate": true,
        "paging": true, 
        "searching": true,
        "order": [[1, "desc"]],
        "aoColumnDefs": [
                    {
                        "targets": [1],
                        "visible": false,
                        "searchable": false

                    }
                ],

         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();  
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);  
        },
    };

    var oTable = $('#mail_display_table').dataTable(tconfig); 
    $(document).off('click', '#generate_btn').on('click', '#generate_btn', function (e) { 
        oTable.fnDraw();     
    }); 
});
</script>