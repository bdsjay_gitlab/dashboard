<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="row">
              <h3 class="box-title" style="margin-left:25px;">Contact Us</h3>
            </div>
            <div class="box-header">
                 <div class="row">
                            <div class="col-md-2">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control" id="from_date" name="from_date" placeholder="From Date" type="text" value="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Till Date</label>
                                    <input class="form-control " id="till_date" name="till_date" placeholder="Till Date" type="text" value=""/>
                                </div>
                            </div>
                             <div class="col-md-2">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-2" style="visibility: hidden;">test</label>
                                        <button type="submit" class="form-control btn btn-primary" id="generate_btn">Submit</button>
                                    </div>  
                             </div>
                    </div>
                                                                           
               
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="mail_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">#</th>
                            <th>Id</th>
                            <th>Date</th>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th width="100px">Action</th>
                        </tr>
                    </thead>
                    <tbody>                                           

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
<script>
  $( function() {
       $( "#from_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
       $( "#till_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
</script>




