<section class="content">
      <div class="row">
        <!-- left column -->
      <form role="form" id="document_collection">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Document </h3>
                <a type="submit" href="<?php echo base_url()?>back/document_collection/allDocument" class="btn btn-primary pull-right" style="background-color:#f0ad4e">All Documents</a>
            </div>
               
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="first_name">Received From *</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" name="coming_from" id="coming_from" placeholder="Enter the source name" value="<?php echo $editdata[0]['coming_from'];?>" required>
                       <input type="hidden" class="form-control" name="id" id="coming_from" placeholder="Received From" value="<?php echo $editdata[0]['id'];?>">
                        <div class="error_comingfrom" style="color:red"> </div>
                   </div>
                </div>
                  <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Address To *</label>
                   <div class="col-sm-5">
                   <?php $usersdata=$this->db->get_where('users',array('society_id'=>$this->session->userdata('society_id'),'role_id !='=> SOCIETY_SUPERUSER))->result_array();
                 ?>
                    <select type="text" class="form-control chosen" placeholder="" aria-controls="allhouse"  style="width: 267px;" id="address_to" name="address_to">
                       
                      <?php $address=$this->db->get_where('users',array('id'=>$editdata[0]['address_to']))->result_array();?>
                      <option value="<?php echo $address[0]['id'];?>"><?php echo $address[0]['first_name']." ".$address[0]['last_name'];?></option>
                      <?php foreach($usersdata as $users){?>
                    <option value="<?php echo $users['id'];?>"><?php echo $users['first_name']." ".$users['last_name'];?></option>
                    <?php }?>
                  </select>
                   <div class="error_addressto" style="color:red"> </div>
                    </div> 
               </div>
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Document Name </label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="document_name" id="document_name" value="<?php echo $editdata[0]['document_name'];?>" maxlength="30" placeholder="Enter document name">
                  </div>
                </div>
                  <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="email">Date *</label>
                    <div class="col-sm-5">
                    <input class="form-control" id="from_date" name="date" placeholder="dd-mm-yyyy" type="text" value="<?php echo $editdata[0]['date'];?>" readonly>
                     <div class="error_date" style="color:red"> </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Handover To *</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="handover_to" id="handover_to" placeholder="Handover To" value="<?php echo $editdata[0]['handover_to'];?>">
                     <div class="error_handoverto" style="color:red"> </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Remark *</label>
                  <div class="col-sm-5">
                    <textarea id="remark" name="remark" rows="8" placeholder="Enter Remark" style="width: 242px;height: 85px;"><?php echo $editdata[0]['remark'];?></textarea>
                     <div class="error_remark" style="color:red"> </div>
                  </div>
                </div>
                <div class="error" style="padding-left: 430px;font-size: 16px;">  </div>
                 <div class="box-footer" style="padding-left: 350px;">
                      <input type="button" class="btn btn-primary add" value="Update Document">
               </div>
             <!--  <div class="box-footer" style="padding-left: 380px;">
                <input type="button" class="btn btn-primary add" value="Update Document">
              </div> -->
            </div>
          </div>
        </div>
            </form>
          </div>
        </div>
      </div>
</section>
<style>
#document_collection input,#address_to
{
      max-width: 61% !important;
}
</style>
<script>
  $( function() {
       $( "#from_date" ).datepicker({ dateFormat: 'yy-dd-mm',maxDate:'0' });
  } );

$(".add").click(function(){
   if($('#coming_from').val()==''){
      $(".error_comingfrom").html('Please enter the source name');
    }else if($('#address_to').val()==' '){
      $(".error_addressto").html('Please select the user');
    }else if($('#from_date').val()==''){
       $(".error_date").html('Please select the date');
    }else if($('#handover_to').val()=='')
    {
       $(".error_handoverto").html('Handover to is required');
    }else if($('#remark').val()==''){
      $(".error_remark").html('Remark is required');
    }
    else{
      var details= $("#document_collection").serialize();
     $.ajax({
      type:'post',
      data:details,
      url:'<?php echo base_url()?>back/document_collection/update_document',
      success:function(res){
        if(res)
        {
           window.location.href='<?php echo base_url()?>back/document_collection/allDocument';
        }
      
      }
    })
    }
  })
</script>