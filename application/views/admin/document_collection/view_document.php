<section class="content">
      <div class="row">
        <!-- left column -->
      <form role="form" id="document_collection" action="<?php echo base_url()?>back/document_collection/allDocument" method="POST">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">View Document Details</h3>
                <a type="submit" href="<?php echo base_url()?>back/document_collection/allDocument" class="btn btn-primary pull-right" style="background-color:#f0ad4e">All Document</a>
            </div>
              
              <div class="box-body">
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="first_name">Coming From *</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" name="coming_from" id="coming_from" placeholder="Enter Coming From Name" value="<?php echo $editdata[0]['coming_from'];?>" readonly>
                       <input type="hidden" class="form-control" name="id" id="coming_from" placeholder="Enter Coming From Name" value="<?php echo $editdata[0]['id'];?>">
                        <div class="error_comingfrom" style="color:red"> </div>
                   </div>
                </div>
                  <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Address To *</label>
                   <div class="col-sm-5">
                   <?php $usersdata=$this->db->get_where('users',array('society_id'=>$this->session->userdata('society_id')))->result_array();?>
                   
                      <?php $address=$this->db->get_where('users',array('id'=>$editdata[0]['address_to']))->result_array();?>
                      <input type="text" class="form-control" name="coming_from" id="coming_from" value="<?php echo $address[0]['first_name']." ".$address[0]['last_name'];?>" readonly>
                
                   <div class="error_addressto" style="color:red"> </div>
                    </div> 
               </div>
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Document Name </label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="document_name" id="document_name" value="<?php echo $editdata[0]['document_name'];?>" readonly>
                  </div>
                </div>
                  <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="email">Date </label>
                    <div class="col-sm-5">
                    <input class="form-control" id="from_date" name="date" placeholder=" Date" type="text" value="<?php echo $editdata[0]['date'];?>" readonly>
                     <div class="error_date" style="color:red"> </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Handover To </label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="handover_to" id="handover_to" placeholder="Enter Handover To" value="<?php echo $editdata[0]['handover_to'];?>" readonly>
                     <div class="error_handoverto" style="color:red"> </div>
                  </div>
                </div>
                 <?php $res=$this->db->get_where('document_collection',array('id'=>$editdata[0]['id']))->result_array();
                                if($res[0]['document_view']=='Y'){?>
                  <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Document Status  </label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="handover_to" id="handover_to" placeholder="Enter Handover To" value="Received" readonly>
                     <div class="error_handoverto" style="color:red"> </div>
                  </div>
                </div>
                <?php }else{?>
                  <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="last_name">Document Status  </label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="handover_to" id="handover_to" placeholder="Enter Handover To" value="Not Received" readonly>
                     <div class="error_handoverto" style="color:red"> </div>
                  </div>
                </div>
                <?php }?>
                <div class="error" style="padding-left: 430px;font-size: 16px;">  </div>
               
                 <div class="box-footer" style="padding-left: 350px;">
                      <input type="submit" class="btn btn-primary add" value="BACK">
                       <?php if($this->session->userdata('role_id') == SOCIETY_MEMBER || $this->session->userdata('role_id') == SOCIETY_ADMIN){?>
                      <?php $res=$this->db->get_where('document_collection',array('id'=>$editdata[0]['id']))->result_array();
                                if($res[0]['document_view']=='Y'){?>
                    
                      <?php }else{?>
                          <a onclick="return confirm('Do you received your document??')" href="<?= base_url().'back/document_collection/readDocument/'.$editdata[0]['id']; ?>" class="btn btn-danger" id="Update">Recieve it</a>
                      <?php }?>
                        <?php }?>
               </div>
             
              
           
            </div>
          </div>
        </div>
            </form>
          </div>
        </div>
      </div>
</section>
<style>
#document_collection input,#address_to
{
      max-width: 61% !important;
}
</style>
