
<script>
 $(document).ready(function() {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  var calendar = $('#calendar').fullCalendar({
   editable: true,
   header: {
    left: 'prev,next today',
    center: 'title',
    right: 'month,agendaWeek,agendaDay'
   },

   events: "<?php echo base_url()?>back/event/allevent?>",

   // eventClick: function(event) {
   //  $('#myModal').modal('show');
   //  $("#title").html(event.title);    
   //   var s = new Date(event.start);
   //   var e = new Date(event.end);
   //    var start = $.fullCalendar.formatDate(event.start, "hh:mm a");
   //    var end = $.fullCalendar.formatDate(event.end, "hh:mm a");
   //   var c = s.toString().split(' ')[0]+","+" "+ s.toString().split(' ')[1]+" "+s.toString().split(' ')[2]+", "+ s.toString().split(' ')[3]+" "+start+" "+"to"+" "+e.toString().split(' ')[0]+","+" "+ e.toString().split(' ')[1]+" "+ e.toString().split(' ')[2]+", "+e.toString().split(' ')[3]+" "+end;
   //   $("#date").html(c);
   //  },
  
 eventMouseover: function (data, event, view) {
  
     //var ts = new Date();
     if(data.start !=null){
      var s = new Date(data.start).toUTCString();
    }else{
     var s='time is not mentioned';
    }
    if(data.end !=null) {
      var e = new Date(data.end).toUTCString();
    }else{
     var e='time is not mentioned';
    }
     // var s = new Date(data.start).toISOString();
     // var e = new Date(data.end).toISOString();

          tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#93e1fa;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 'title: ' + ': ' + data.title + '</br>' + 'start: ' + ': ' +s+'<br>' +'end'+ ' : '+e+'</div>';



            $("body").append(tooltip);
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY + 10);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });


        },
        eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);

            $('.tooltiptopicevent').remove();

        },
        dayClick: function () {
            tooltip.hide()
        },
        eventResizeStart: function () {
            tooltip.hide()
        },
        eventDragStart: function () {
            tooltip.hide()
        },
        viewDisplay: function () {
            tooltip.hide()
        },




  }); 
 });
</script>
<style>
 body {
  font-size: 14px;
  font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
  }
 #calendar {
  width: 800px;
  margin: 0 auto;
  }
</style>
</head>
<body>

 <h2 style="padding:20px">Events</h2>
 <br/>
 <div id='calendar'></div>
<!--  -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
         <div class="alert alert-success">
           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <div id="title" style="font-size: 25px;text-align: center"></div>
       <hr>
      <span class="glyphicon glyphicon-time" aria-hidden="true" style="font-size: 22px"></span> <div id="date" style="color: white;margin-top:-25px;margin-left:30px"></div><br>

      <span class="glyphicon glyphicon-calendar" aria-hidden="true" style="font-size: 18px"></span><div style="color: white;margin-top:-25px;margin-left:30px">Events For Society</div><br>
       <span class="glyphicon glyphicon-lock" aria-hidden="true" style="font-size: 18px"></span><div style="color: white;margin-top:-25px;margin-left:30px">Public</div>
    </div>
  </div>
</div>
<!--  -->
</body>
<style>
.alert-success {
 width:450px !important;
 margin-top:450px;
  }
  .alert-success {
    background-color: #337ab7 !important;
  }
   body .fc {
    border: 5px solid #337ab7 !important;
}

</style>
</html>