<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Add Facility
                                     <hr>
                                </h3>
                            </div>
                        </div>
                        <div class="box-body">
                         
                            <form id="facility_member" name="facility_member" method="post" action="<?php echo  base_url().'back/facility/add_facility'?>">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Facility Name</label>
                                        <div class="col-lg-4"> 
                                            <input type="text" class="form-control" name="facility_name" id="facility_name" placeholder="Please Enter Facility Name" maxlength="25">
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Adult Charges</label>
                                        <div class="col-lg-4"> 
                                            <input type="text" class="form-control" name="adult_charges" id="adult_charges" placeholder="eg. 100">
                                        </div>
                                </div>
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="adult_count">Child Charges</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="child_charges" id="child_charges" placeholder="eg. 100">
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <!-- <div class="form-group">
                                    <label class="col-lg-2 control-label" for="child_count">Maintenance Cost</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="maintenance_cost" id="maintenance_cost" placeholder="Please Enter Maintenance Cost">
                                        </div>
                                </div> -->
                             
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-1 control-label" for="total_fees"></label>
                                        <div class="col-lg-5">
                                            <div class="box-footer" style="padding-left: 80px;" align="center">
                                                <button type="submit" class="btn btn-primary">Add Facility</button>
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

<script type="text/javascript">
  $(document).ready(function(){
    $('#facility_member').validate({
      rules:{
        facility_name:{
          required:true,
          // lettersonly:true
        },
        adult_charges:{
          digits:true,
          maxlength:10
        },
        child_charges:{
          digits:true,
          maxlength:10
        },
        maintenance_cost:{
          required:true,
          digits:true,
          maxlength:15
        }
      },
      messages:{
        facility_name:{
          required:"Please Enter Facility Name",
          // lettersonly:"Please Enter Valid Facility Name"
        },
        adult_charges:{
          digits:"Please Enter Valid Adult Charges"
        },
        child_charges:{
          digits:"Please Enter Valid Child Charges"
        },
        maintenance_cost:{
          required:"Please Enter Maintenance Cost",
          digits:"Please Enter Valid Maintenance Cost"
        }
      }
    });
  });
</script>