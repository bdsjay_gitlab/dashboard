<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Allocate Facility
                                     <hr>
                                </h3>
                            </div>
                        </div>
                        <div class="box-body">
                         
                            <form id="facility_member" name="facility_member" method="post" action="<?php echo  base_url().'back/facility/addMember'?>">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Facility</label>
                                        <div class="col-lg-4"> 
                                            <?php $res=$this->db->get_where('facility_master',array('society_id'=>$this->session->userdata('society_id'),'is_deleted' => 'N'))->result();?>
                                            <select class="facility form-control" name="facilty_id" id="facilty_id" required="">
                                            <option value="">Select Facility</option>
                                            <?php foreach($res as $facility){?>
                                                <option value="<?= $facility->id ?>"><?= $facility->facility_name; ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Users</label>
                                        <div class="col-lg-4"> 
                                            <select class="users form-control" name="user_id" id="user_id" required="">
                                            <option value="">Select Users</option>
                                            <?php if($users){?>
                                            <?php foreach($users as $key => $value){?>
                                            <option value="<?php echo $value->id?>">
                                                <?php echo $value->user_name.' ('.$value->building.' '.$value->wing.' '.$value->block.')'; ?>
                                            </option>
                                            <?php }?>
                                            <?php }?>
                                            </select>
                                        </div>
                                </div>
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="adult_count">Adult Count</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control adult_child" name="adult_count" id="adult_count" placeholder="Please Enter Adult Count" value="">
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="child_count">Child Count</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control adult_child" name="child_count" id="child_count" placeholder="Please Enter Child Count" value="">
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="total_member">Total Members</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="total_member" placeholder="Please Enter Total Member" id="total_member" value="" readonly>
                                              <div id="count" style="color: red"></div>
                                        </div>
                               </div>
                             
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-1 control-label" for="total_fees"></label>
                                        <div class="col-lg-5">
                                            <div class="box-footer" style="padding-left: 80px;" align="center">
                                                <button type="submit" class="btn btn-primary" id="add_member">Allocate Facility</button>
                                                  <a href="<?php echo base_url();?>back/facility/allMember" class="btn btn-primary add_btn pull-right"><i class="fa fa-arrow-left">&nbsp;Back</i></a>
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

<script type="text/javascript">
    $(document).ready(function () {

    $("#facility_member").validate({
        rules: {
            adult_count: {
                required: true,
                number:true,
                maxlength:4
            },
            child_count: {
                number: true,
                maxlength:4
            }
        },

        errorPlacement: function (error, element) {
                   
            if(element.attr("name") === "facilty_id") {
                error.appendTo(element.parent("div") );
            } else if(element.attr("name") === "user_id") {
                error.appendTo(element.parent("div") );
            } else {
                error.insertAfter(element);
            }
                
        },
        messages: {        
            adult_count: {
                required: "Please Enter Numbers Only",
                number: "Please Enter valid Adult Count",
                maxlength:"Maximum 4 Digit Allowed"
            },
            child_count: {
                number: "Please Enter valid Child Count",
                maxlength:"Maximum 4 Digit Allowed"
            }
        }
    });
});
</script>
<script type="text/javascript">
     $(document).ready(function(){
        var adult_count = '';
        var child_count = '';
        var total='';
        $('#adult_count').blur(function(){
             adult_count = $('#adult_count').val();
             child_count = $('#child_count').val();
              if(adult_count==''){
                adult_count=0;
             }
              if(child_count==''){
                child_count=0;
             }
        });

        $('#child_count').blur(function(){
             adult_count = $('#adult_count').val();
             child_count = $('#child_count').val();
              if(adult_count==''){
                adult_count=0;
             }
              if(child_count==''){
                child_count=0;
             }
        });
        
        $('#child_count').on('foucusout blur',function() {
            total = parseInt(child_count) + parseInt(adult_count);
            if(total == 0){
                $('#count').html('Both count should not be zero');
            } else {
                $('#count').html('');
            }
            $('#total_member').val(total);
        });

         $('#adult_count').on('foucusout blur',function(){
            total = parseInt(child_count) + parseInt(adult_count);
            if(total == 0){
                $('#count').html('Both count should not be zero');
            } else {
                $('#count').html('');
            }
            $('#total_member').val(total);
        });
    });
</script>

<script type="text/javascript">
    $(".adult_child").on("keypress",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
</script>

<script type="text/javascript">
     $(document).off('change','#facilty_id').on('change','#facilty_id', function() {
        
        var facility_id = this.value;
        
            $.ajax({
              url: '<?php echo base_url();?>back/facility/check_value_exist',
              type:'POST',
              data:{facility_id:facility_id},
              success:function(res)
              {
                 response = JSON.parse(res);
                 var adult_status = response.adult;
                 var child_status = response.child;
                 
                 $("#child_count").attr("disabled", false);
                 $("#adult_count").attr("disabled", true);
                 
                 if(adult_status == 'true' && child_status == 'true')
                 {
                     $("#child_count").attr("disabled", false);
                     $("#adult_count").attr("disabled", false);
                 }
                 else if(adult_status == 'false' && child_status == 'false')
                 {
                      $("#child_count").attr("disabled", false);
                      $("#adult_count").attr("disabled", false);
                 }else if(adult_status == 'false' && child_status == 'true')
                 {
                       $("#child_count").attr("disabled", false);
                       $("#adult_count").attr("disabled", true);
                 }
                 else if(adult_status == 'true' && child_status == 'false')
                 {
                        $("#child_count").attr("disabled", true);
                        $("#adult_count").attr("disabled", false);
                 }
              }
          });
    });
</script>