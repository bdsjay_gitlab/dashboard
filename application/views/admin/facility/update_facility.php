<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Edit Facility
                                     <hr>
                                </h3>
                            </div>
                        </div>
                        <div class="box-body">
                         
                            <form role="form" method="post" action="<?= base_url();?>back/Facility/edit_facility" id="user_form" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="facility_name">Facility Name</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="num form-control" name="facility_name" id="facility_name" placeholder="Please Enter Facility Name" value="<?php echo $edit[0]->facility_name?>">
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="adult_charges">Adult Charges</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="num form-control" name="adult_charges" id="adult_charges" placeholder="Please Enter Adult Charges" value="<?php echo $edit[0]->adult_charges?>">
                                        </div>
                                </div>
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="child_charges">Child Charges</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="num form-control" name="child_charges" id="child_charges" placeholder="Please Enter Child Charges" value="<?php echo $edit[0]->child_charges?>">
                                        </div>
                                        <input type="hidden" name="fac_id" value="<?php echo $edit[0]->id?>" placeholder="">
                                </div>
                                <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="maintenance_cost">Maintenance Cost</label>
                                        <div class="col-lg-6">
                                            <input type="text" class="num form-control" name="maintenance_cost" id="maintenance_cost" placeholder="Please Enter Maintenance Cost" value="<?php echo $edit[0]->maintenance_cost?>">
                                        </div>
                                </div> -->
                             
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-1 control-label" for="total_fees"></label>
                                        <div class="col-lg-5">
                                            <div class="box-footer" style="padding-left: 80px;" align="center">
                                                <button type="submit" class="btn btn-primary" id="update_fac">update Facility</button>
                                                &nbsp;
                                                 <a class="btn btn-danger" href="<?php echo  base_url().'back/societycharges'?>" type="button">Back</a> 
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

<script type="text/javascript">
  $(document).ready(function(){
    $('#user_form').validate({
      rules:{
        facility_name:{
          required:true,
        },
        adult_charges:{
          digits:true,
          maxlength:10
        },
        child_charges:{
          digits:true,
          maxlength:10
        },
        maintenance_cost:{
          required:true,
          digits:true,
          maxlength:15
        }
      },
      messages:{
        facility_name:{
          required:"Please Enter Facility Name"
        },
        adult_charges:{
          digits:"Please Enter Valid Adult Charges"
        },
        child_charges:{
          digits:"Please Enter Valid Child Charges"
        },
        maintenance_cost:{
          digits:"Please Enter Valid Maintenance Cost"
        }
      }
    });
  });
</script>