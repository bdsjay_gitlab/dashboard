<?php //echo '<pre>';print_r($facilities);?>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Add Society Member
                                     <hr>
                                </h3>
                            </div>
                        </div>
                        <div class="box-body">
                         
                               <div class="form-group">
                                    <label class="col-lg-2 control-label">Facility</label>
                                    <?php $users=$this->db->get_where('facility_master',array('id'=>$userFacility[0]->facilty_id))->result()?>
                                       <div class="col-lg-4">
                                            <input type="text" class="num form-control" name="adult_count" id="adult_count" value="<?php echo $users[0]->facility_name;?>" readonly>
                                        </div>
                                </div>
                               <!--  <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Users</label>
                                       <div class="col-lg-4">
                                            <input type="text" class="num form-control" name="adult_count" id="adult_count" value="<?php echo $userFacility[0]->adult_count?>" readonly>
                                        </div>
                                </div> -->
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Users</label>
                                        <div class="col-lg-4"> 
                                            <input type="text" class="num form-control" name="name" id="name" value="<?php echo $user_drop[0]->first_name." ".$user_drop[0]->last_name;?>" readonly>
                                        </div>
                                </div>
                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="adult_count">Adult Count</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="num form-control" name="adult_count" id="adult_count" value="<?php echo $userFacility[0]->adult_count?>" readonly>
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="child_count">Child Count</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="num form-control" name="child_count" id="child_count" value="<?php echo $userFacility[0]->child_count?>" readonly>
                                        </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="total_member">Total Members</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="total_member" id="total_member" value="<?php echo $userFacility[0]->total_member?>" readonly>
                                        </div>
                               </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="total_member">Total Fees</label>
                                        <div class="col-lg-4">
                                            <input type="text" class="form-control" name="total_member" id="total_member" value="<?php echo $userFacility[0]->total_fees?>" readonly>
                                        </div>
                               </div>
                             
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-1 control-label" for="total_fees"></label>
                                        <div class="col-lg-5">
                                            <div class="box-footer" style="padding-left: 80px;" align="center">
                                                <a href="<?php echo  base_url().'back/facility/allmember'?>">
                                                <button type="submit" class="btn btn-danger" id="add_member">Back</button>
                                            </a>
                                            </div>
                                        </div>
                                </div>
                        </div>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->

<script type="text/javascript">
    $(document).ready(function () {

    $("#facility_member").validate({
        rules: {
            adult_count: {
                required: true,
                number:true
            },
            child_count: {
                required: true,
                number: true
            }
        },

        errorPlacement: function (error, element) {
                   
            if(element.attr("name") === "facilty_id") {
                error.appendTo(element.parent("div") );
            } else if(element.attr("name") === "user_id") {
                error.appendTo(element.parent("div") );
            } else {
                error.insertAfter(element);
            }
                
        },
        messages: {        
            adult_count: {
                required: "Please Enter Numbers Only",
                number: "Please Enter valid Adult Count"
            },
            child_count: {
                required: "Please Enter Numbers Only",
                number: "Please Enter valid Child Count"
            }
        }
    });
});

</script>
<script type="text/javascript">
     $(document).ready(function(){
        
        $('.facility').change(function() {
            var facility = $('.facility').val();
        }); 

        $('.users').change(function() {
            var users = $('.users').val();
        });
        
        $('#child_count').keyup(function(){
            var child_count = $('#child_count').val();
            var adult_count = $('#adult_count').val();
            var total=parseInt(child_count) + parseInt(adult_count);
            $('#total_member').val(total);
        });
    });
</script>