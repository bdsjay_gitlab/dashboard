<section class="content">
      <div class="row">
        <!-- left column -->
      <form role="form" method="post" action="<?= base_url();?>back/registration/addhouse" enctype="multipart/form-data" id="house_form">
         <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add House</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_name">Society Name *</label>
                  <div class="col-sm-5">
                    <select name="society_name" class="form-control" id="society_name">
                       <?php if($this->session->userdata('role_id')==SUPERADMIN){?><option value="">Select Society</option>
                      <?php }?>
                      <?php foreach($society as $society) {?>
                      <option value="<?= $society->id; ?>"><?= $society->name; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="building">Building</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="building" id="building" placeholder="Enter building Name Or No.">
                  </div>
                </div>
                 <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="wing">Society Wing</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="wing" id="wing" placeholder="Enter Wing">
                    <p class="help-block">For example: A</p>
                  </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="block">Block / Flat Number *</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" name="block" id="block" placeholder="Block / Flat Number">
                    <p id="existingHouse" style="color: red"></p>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="house">House Type *</label>
                    <div class="col-sm-5">
                      <select name="house_type" class="form-control" id="city">
                        <option value="">Select House Type</option>
                        <?php if($houseType){
                         foreach($houseType as $h_type){?>
                            <option value="<?= $h_type->id?>"><?= $h_type->house_type; ?></option>
                        <?php }} ?>
                      </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label col-sm-offset-1" for="on_lease">House On Lease *</label>
                    <div class="col-sm-5">
                      Yes&nbsp;<input type="radio" name="on_lease" value="Y">&nbsp;&nbsp;
                      No&nbsp;<input type="radio" name="on_lease" value="N" checked>
                    </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="house">Details</label>
                  <div class="col-sm-5">
                    <input type="text" name="details" class="form-control" placeholder="Enter Other Details"/>
                  </div>
               </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="profile_pic">House Image</label>
                  <div class="col-sm-5">
                  <input type="file" name="house_pic" id="house_pic">
                  <p class="help-block">Your House Picture.(Only png,jpg,jpeg and gif allowed)</p>
               </div>
             </div>
               <div class="box-footer" style="padding-left: 350px;">
                  <button type="submit" class="btn btn-primary">Add House To Society</button>&nbsp;&nbsp;
                  <a href="javascript:window.history.go(-1);" class="btn btn-danger"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
               </div>
              </div>
            </div>
          </div>
      </form>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  $('#block').focusout(function(){
     var block     = $('#block').val();
     var wing      = $('#wing').val();
     var building  = $('#building').val();
    $('#existingHouse').empty();
  $.ajax({
    url : '<?= base_url(); ?>back/registration/houseExist',
    type: 'POST',
    data: {block:block,wing:wing,building:building},
    success:function(res) {
      $(res).appendTo('#existingHouse');
      $('#block').attr("aria-invalid",true); //house valid check
    }
  });
  });
});
</script>