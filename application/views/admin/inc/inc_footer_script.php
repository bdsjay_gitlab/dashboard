<script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Event -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
<!-- End event -->
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="<?php echo base_url();?>assets/dist/js/pages/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url();?>assets/plugins/jquery-notifications/js/messenger.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-notifications/js/messenger-theme-future.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
<!-- Multiselect js /css start  -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<!-- Multiselect js /css end  -->
<script src="<?php echo base_url().'assets/plugins/jQueryUI/jquery-ui.js'?>"></script>
<!-- <script src="/javascripts/application.js" type="text/javascript" charset="utf-8" async defer></script> -->
<!-- Jquery Validation start-->
<script src="<?php echo base_url()?>assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.1/jquery.form-validator.min.js"></script>
<!-- Jquery Validation end -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">


<script src="<?php echo base_url();?>assets/custom_file/custom_js.js"></script>
<script src="<?php echo base_url();?>assets/custom_file/my_validation.js"></script>
<!-- start timepicker -->
<script src="<?php echo base_url()?>assets/plugins/timepicker/jquery.ptTimeSelect.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    // $('.sidebar-menu').hide();
    $('.main-sidebar').removeClass('hidden');
    var msg_type = "<?php echo $this->session->flashdata('msg_type');?>";
    var msg = "<?php echo $this->session->flashdata('msg');?>";

    if(msg_type != "" && msg != "")
    {
       $.bootstrapGrowl(msg, { type: msg_type });
    }
  });
    function showLoader(msg_type,msg)
    {
        $.bootstrapGrowl(msg,msg_type);
    }

</script>
<script type="text/javascript">
$(function() {
    $(".chosen").chosen();
});
</script>