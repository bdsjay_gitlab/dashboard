<aside class="main-sidebar hidden">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image" style="padding-bottom: 20px;">
          <?php if($this->session->userdata('profile_pic')){?>
           <img src="<?php echo base_url();?>upload/profileImages/<?php echo $this->session->userdata('profile_pic')?>" class="img-circle" alt="User Image"> 
          <?php }else{?>
          <img src="<?php echo base_url();?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          <?php }?>
        </div>
        <div class="pull-left info">
          <p><?php 
          $name = ucfirst($this->session->userdata('first_name'))." ".ucfirst($this->session->userdata('last_name'));

          if($this->session->userdata('role_id') == SOCIETY_SUPERUSER)
          {
            $name = ucfirst($this->session->userdata('first_name'));
          }
          echo wordwrap($name, 20, "<br />\n");
          ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
     <!--  <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
       <ul class="sidebar-menu">
             <li>
                   <a href="<?php echo base_url().'admin/dashboard'?>">
                       <i class="fa fa-dashboard"></i>
                       <span>Dashboard</span>
                   </a>
            </li>
             <?php echo get_back_menu(); ?>
    </section>
    <!-- /.sidebar -->
  </aside>
    <script>
    $(window).bind("load", function() {
        var urlmenu = window.location.pathname;
        if(urlmenu.substr(-1) == '/') {
            urlmenu = urlmenu.substr(0, urlmenu.length - 1);
        }
        var rest_url = urlmenu.substring(0, urlmenu.lastIndexOf("/") + 1);

        var url_last_part = urlmenu.substring(urlmenu.lastIndexOf("/") + 1, urlmenu.length);
        $('.sidebar-menu li a').each(function(){
            var sidebar_href = $(this).attr("href");
            if(sidebar_href.substr(-1) == '/') {
                sidebar_href = sidebar_href.substr(0, sidebar_href.length - 1);
            }
        
            var sidebar_rest_url = sidebar_href.substring(0, sidebar_href.lastIndexOf("/") + 1);
            var sidebar_url_last_part = sidebar_href.substring(sidebar_href.lastIndexOf("/") + 1, sidebar_href.length);
            if(sidebar_url_last_part == url_last_part)
            {
                $(this).parent("li").addClass('active');
                $(this).parents("li").addClass('active');
                $(this).parent("li").parents("ul").css({"display":"block"}); 
            }
        });
    })
</script>