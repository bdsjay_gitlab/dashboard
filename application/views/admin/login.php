<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BDS Serv</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">
 <link rel="shortcut icon" href="<?= base_url()?>assets/images/favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?= base_url()?>assets/images/favicon.ico" type="image/x-icon">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style>
/* {
  
  background-attachment: fixed;
  background-repeat: repeat-y;
  font-size: 15px;
  color: #fff;
  margin: 0 auto;
  overflow-y: hidden;
  overflow-x: hidden; 
}*/
  body
  { 
    background:url(assets/images/background.jpg) top right no-repeat;
    background-attachment: fixed;
    min-width: 100%;
    min-height: 100%;
    background-position: center;
    background-size: cover;
   }
.main {
  text-align: center;
  background: rgba(255,255,255,0.3);
  margin-top:5%;
  margin-left: 35%;
  margin-right: 35%;
  padding: 20px;
  border-radius: 10px;
}
img{
  max-height: 500px;
}
@media screen and (max-width: 1024px) {
    .main {
        /*margin-top:100px;*/
        width: 90%;
        margin-left: 5% !important;
        margin-right: 5% !important;
        text-align: center !important;
    }
}
</style>
</head>
<body>
  <div class="row"> 
    <div class="col-md-12 col-sm-12 col-lg-12"> 
<div class="main">
  <div class="login-logo">
    <a href="#">
      <img class="img-responsive center-block" src="<?php echo base_url()?>assets/images/hs_4-01-for_site_1024.png" alt="BDS Logo" style="height:170px;">
      <hr>
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" id="login">
    <p class="login-box-msg">Login To Your Account</p>
 
    <?php 
    if(isset($error)){ ?>
  <div class="error" style="width:100%;margin-bottom:5px;"><?php echo $error;?></div>
    <?php } ?>

    <form action="<?php echo base_url();?>home/login_authencticate" method="post" id="login_form">
      <div class="form-group has-feedback">
        <input type="input" class="form-control" name="username" placeholder="Username *" style="border-radius: 12px" />
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password *" id="password" style="border-radius: 12px"/><br>
        <input type="checkbox" id="pass1"> &nbsp;Show Password&nbsp;&nbsp;
        <a href="#" id="fpass">Forget Password</a>
        <span class="fa fa-key form-control-feedback"></span>
      </div>
     
      <div class="row">
        <div class="col-md-4 col-sm-12 text-center">
          <button type="submit" id="signin" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- Forget Password -->

    <div class="login-box-body" id="forget_pass">
    <p class="login-box-msg">Forgot your password ?</p>
 
    <?php 
    if(isset($error)){ ?>
  <div class="error" style="width:100%;margin-bottom:5px;"><?php echo $error;?></div>
    <?php } ?>

    <!-- <form action="<?php //echo base_url();?>admin/forgetpass" method="post" id="reset_pass"> -->
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="usr_name" id="usr_name" placeholder="Enter Username *" />
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-md-4 col-sm-12 text-center">
          <button type="button" class="btn btn-primary btn-block btn-flat" id="forget">Get Email</button>
        </div>
        <div class="col-md-4 col-sm-12 text-center">
          <a href="#" id="back1">Back</a>
        </div>
        <!-- /.col -->
      </div>
  </div>
  <!-- Change Password -->

   <div class="login-box-body" id="change_pass">
    <p class="login-box-msg">Update Password</p>
 
    <?php 
    if(isset($error)){ ?>
  <div class="error" style="width:100%;margin-bottom:5px;"><?php echo $error;?></div>
    <?php } ?>

<!--     <form action="<?php //echo base_url();?>admin/changepass" method="post" id="rst_form"> -->
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="otp" id="otp" placeholder="OTP from your registred email *" />
        <span class="fa fa-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="ch_pass" placeholder="Password *" id="ch_pass" />
        <span class="fa fa-key form-control-feedback"></span><br>
        <input type="checkbox" id="pass"> &nbsp;Show Password&nbsp;&nbsp;
      </div>
     
      <div class="row">
        <div class="col-md-4 col-sm-12 text-center">
          <button type="button" class="btn btn-primary" id="upass">Update Password</button>
        </div>
        <!-- /.col -->
      </div>
<!--     </form> -->
  </div>
  <p style="color:#000000;padding-top:10px; ">Copyright &copy; <?= date('Y'); ?> <a href="https://bdsserv.com" style="color: #000000;">BDS STZ Techno Systems Pvt. Ltd. </a>All Rights Reserved.</p>
  </div>
</div>
</div>
<script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- bootstrap growl -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

  var msg_type = "<?php echo $this->session->flashdata('msg_type'); ?>";
  var msg      = "<?php echo $this->session->flashdata('msg'); ?>";
 
  if(msg_type != "" && msg != "")
  {
     $.bootstrapGrowl(msg, { type: msg_type });
  }

  $('#forget_pass').hide();
  $('#change_pass').hide();
  $('#fpass').click(function(){
      $('#forget_pass').show();
      $('#change_pass').hide();
      $('#login').hide();
  });
  $('#back1').click(function(){
    $('#forget_pass').hide();
    $('#change_pass').hide();
    $('#login').show();
  });
  $('#forget').click(function(){
    $('#forget_pass').hide();
    $('#login').hide();
    $('#change_pass').show();
  });
  // $('#login_form').validate({
  //   rules:{
  //     username:{
  //       required:true
  //     },
  //     password:{
  //       required:true
  //     }
  //   },
  //   messages:{
  //     username:{
  //       required:"Please Enter User Name"
  //     },
  //     password:{
  //       required: "Please Enter Password"
  //     }
  //   }
  // });

    // $(function () {
  //   $('input').iCheck({
  //     checkboxClass: 'icheckbox_square-blue',
  //     radioClass: 'iradio_square-blue',
  //     increaseArea: '20%' // optional
  //   });
  // });

  $('#signin').click(function(){

    $('#forget_pass').hide();
    $('#change_pass').hide();
  });
  
  $('#login_form :checkbox').change(function() {
      
    if (this.checked) {
        $('#password').attr('type','text');
    } else {
        $('#password').attr('type','password');
       }
  });

  $('#change_pass :checkbox').change(function() {
      
    if (this.checked) {
        $('#ch_pass').attr('type','text');
    } else {
        $('#ch_pass').attr('type','password');
       }
  });

  $('#forget').click(function(){       

    var usr_name = $('#usr_name').val();
      $.ajax({
        url:'<?php echo base_url(); ?>admin/forgetPass',
        type:'POST',
        data:{usr_name:usr_name},
        success:function(res){
           obj = JSON.parse(res);
           var msg_type = obj.msg_type;
           var msg = obj.msg;
           if(msg_type && msg)
           {
                $.bootstrapGrowl(msg, { type: msg_type });
           }
        }
      });
  });

  $('#upass').click(function(){       

    var password = $('#ch_pass').val();
    var otp      = $('#otp').val();

      $.ajax({
        url:'<?php echo base_url(); ?>admin/changePass',
        type:'POST',
        data:{password:password,otp:otp},
        success:function(res){
          obj = JSON.parse(res);
           var msg_type = obj.msg_type;
           var msg = obj.msg;
           if(msg_type && msg)
           {
                $.bootstrapGrowl(msg, { type: msg_type });
           }
            setTimeout(function(){
               window.location.reload(1);
            }, 3000);
          }
        });
    });
});
</script>
</body>
</html>
