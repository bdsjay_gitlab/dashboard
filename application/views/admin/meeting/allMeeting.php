<section class="content">
    <div class="row">
 		<div class="col-md-12 col-sm-12 col-xs-12">
 			<h3>Meeting List</h3>
 			<div class="box box-primary" id="box-primary" style="padding: 15px">
 				<div class="table-responsive">
 				<table id="allmeeting" class="table table-condensed table-hover table-responsive">
 					<thead>
 						<tr>
 							<th>ID</th>
 							<th>Meeting Date</th>
 							<th>Society Name</th>
 							<th>Purpose</th>
 							<th>Start At</th>
 							<th>End At</th>
 							<th>Action</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php 
 						$i=1;
 						foreach($meeting as $meeting){
 							$id = $meeting->id; ?>
 						<tr>
 							<td><?= $i;?></td>
 							<td><?= date('d-m-Y',strtotime($meeting->meeting_date));?></td>
 							<td><?= $meeting->name;?></td>
 							<td><?= $meeting->subject;?></td>
 							<td><?= $meeting->start_time;?></td>
 							<td><?= $meeting->end_time;?></td>
 							<?php if($this->session->userdata('role_id') == SOCIETY_SUPERUSER || $this->session->userdata('role_id') == SUPERADMIN){?>
 							<td><!-- <a href="<?= base_url().'back/meeting/editmeeting/'.$id; ?>" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit" id="edit" style=""><i class="fa fa-pencil"></i></a> |  --><a href="<?= base_url().'back/meeting/showmeeting/'.$id; ?>" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="View" id="Update"><i class="fa fa-eye"></i></a> | <a href= "<?= base_url().'back/meeting/softDeletemeeting/'.$meeting->meeting_id; ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete" id="delete" onclick="return confirm('Are you sure want to delete this house permanently?')"><i class="fa fa-trash"></i></a></td>
 							<?php }else{?>
 							<td>
                                 <a href="<?= base_url().'back/meeting/showmeeting/'.$id; ?>" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="View" id="Update"><i class="fa fa-eye"></i></a>
                                 </td> 
 							<?php } ?>
 						</tr>
 						<?php $i++; } ?>
 					</tbody>
 				</table>
 				</div>
 			  </div>
 			</div>
      </div>
 </section>
