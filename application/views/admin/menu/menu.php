<section class="content">
    <div id="menu_wrapper">
        <?php
        $this->load->view(MENU_AJAX);
        ?>
    </div>

    <?php $this->load->view(MENU_POPUP); ?>
</section>
<script src="<?php echo base_url()?>assets/common_js/admin_menu.js"></script>
