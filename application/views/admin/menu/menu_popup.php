<div class="modal fade popup" id="add_edit_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Menu</h4>
            </div>

            <?php
            $attributes = array("method" => "POST", "id" => "menu_form", "name" => "menu_form");
            echo form_open('', $attributes);
            ?>
            <div class="modal-body">
                <div class="form-group">
                    <label for="parent">Parent:</label>
                    <select name="parent" id="menu_parent" class="chosen_select form-control"  data-rule-required="true" data-rule-fullname="true" data-msg-required="Please select parent" required>
                        <option value = ''>Select Parent</option>
                        <option value = '0'>Root</option>
                        <?php
                        foreach ($menu_array as $value) {
                            echo "<option value = '" . $value->id . "'>" . ucwords($value->display_name) . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Menu Name:</label>
                    <input type="text" placeholder="Name" name="menu_name" id="menu_name" class="form-control" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter menu name" required/>
                </div>
                <div class="form-group">
                    <label for="link">Link:</label>
                    <input type="text" placeholder="Link" id="menu_link" name="menu_link" class="form-control"  data-rule-required="true" data-msg-required="Please enter menu link" required/>
                </div>
                <div class="form-group">
                    <label for="class">Class:</label>
                    <input type="text" placeholder="Class" id="menu_class" name="menu_class"  class="form-control"/>
                    <input name="id" type="hidden" id="id" class="form-control"/>
                </div>
            </div>
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>

                <button type="button" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Save Menu</button>
            </div>
            <?php echo form_close(); ?>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
