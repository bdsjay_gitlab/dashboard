<section class="content">
    <div id="datatable_wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Menu Sequence</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="example" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="35px">#</th>
                                    <th>Menu Name</th>
                                    <th>Link</th>
                                    <th width="150px">Record Status</th>
                                </tr>
                            </thead>
                            <tbody id="sortable">                                           
                                <?php
                                if (count($menu_parent) > 0) {
                                    $count = 0;
                                    foreach ($menu_parent as $menu) {
                                        $count++;
                                        echo "<tr class='ui-state-default' id='" . $menu["id"] . "' >";
                                        echo "<td>" . $count . "</td>";
                                        echo "<td>" . $menu["display_name"] . "</td>";
                                        echo "<td>" . $menu["link"] . "</td>";
                                        echo "<td>" . $menu["record_status"] . "</td>";
                                        echo "</tr>";
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </div>
</section>
<script>
    
    $(document).ready(function(){
        $('#sortable').sortable({
            update: function(event, ui) {
                var newOrder = $(this).sortable('toArray').toString();
               $.post("<?php echo base_url(); ?>back/menu/menu_resequence", {"new_sequence":newOrder}, function(data){
                  if(data.trim() == "success")
                  {
                      showLoader("success","Successfully updated menu sequence");
                  }
                  else
                  {
                      showLoader("danger","Could not updated menu sequence. Please try again.");
                  }
               });
            }
        });
    });
    
</script>