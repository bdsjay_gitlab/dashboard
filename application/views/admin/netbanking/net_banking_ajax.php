<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">List Of Netbanking Details</h3>
                <?php //if($this->session->userdata('role_id')==SOCIETY_SUPERUSER){?>
                <span style="float: right;margin-top: 10px;margin-right: 10px;"><button class="btn btn-primary net_banking_btn">Add Net Banking</button></span>

                <?php //} ?>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="mail_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="35px">Sr No</th>
                            <th>Id</th>
                            <th>Account Name</th>                          
                            <th>Account No</th>
                            <th>Bank Name</th>
                            <th>IFSC Code</th>
                            <th>Branch Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody> 
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
<script type="text/javascript">
 function doconfirm()
{
    job=confirm("Are you sure to delete permanently?");
    if(job!=true)
    {
        return false;
    }
}   
</script>


