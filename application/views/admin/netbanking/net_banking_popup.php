<div class="modal fade popup" id="net_banking_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php
            $attributes = array("method" => "POST", "id" => "net_banking_form", "name" => "net_banking_form");
            echo form_open(base_url().'back/netbanking/add_net_banking', $attributes);
            ?>
            <div class="modal-body">
                 <div class="form-group">
                    <div class="form-group">
                        <label for="title">Account Name:</label>
                        <input type="text" placeholder="Account Name" name="acc_name" id="acc_name" class="form-control" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label for="title">Account Number:</label>
                        <input type="text" placeholder="Account Number" name="acc_no" id="acc_no" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label for="title">Bank Name:</label>
                        <input type="text" placeholder="Bank Name" name="bank_name" id="bank_name" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label for="title">IFSC Code:</label>
                        <input type="text" placeholder="IFSC Code" name="ifsc_code" id="ifsc_code" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label for="title">Branch Name:</label>
                        <input type="text" placeholder="Branch Name" name="branch" id="branch" class="form-control"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Cancel</button>

                <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i>Save</button>
            </div>
            <?php echo form_close();?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  // /[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$/
  $(document).ready(function(){
    $('#net_banking_form').validate({
      rules:{
        acc_name:{
          required:true,
          
        },
        acc_no:{
          required:true,
          number:true
        },
        bank_name:{
          required:true,
        },
        ifsc_code:{
          required:true
        },
        branch:{
          required:true
        }
      },
      messages:{
        acc_name:{
          required:'Please Enter Account Name',
          
        },
        acc_no:{
          required:'Please Enter Valid Account Number',
          number:'Please Enter Only Digits'
        },
        bank_name:{
          required:'Please Enter Bank Name'
        },
        ifsc_code:{
          required:'Please Enter Valid IFSC Code'
        },
        branch:{
          required:'Please Enter Branch Name'
        }
      }
    });
    jQuery.validator.addMethod("bank_name", function(value, element) {
  // allow any non-whitespace characters as the host part
  return this.optional( element ) || /^[a-zA-Z ]+$/.test( value );
}, 'Please enter a valid bank name.');

     jQuery.validator.addMethod("acc_name", function(value, element) {
  // allow any non-whitespace characters as the host part
  return this.optional( element ) || /^[a-zA-Z ]+$/.test( value );
}, 'Please enter a valid account name.');
  });
</script>