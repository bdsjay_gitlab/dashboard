<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                   Edit Parking Details
                                     <hr>
                                </h3>
                            </div>
                        </div>
                      <div class="box-body">
                         
                  <form id="payment_request" name="payment_request" method="post" action="<?php echo base_url();?>back/parking/update/">
                    <div class="form-group">
                      <input type="hidden" name="p_id" value="<?php echo $p_id;?>">
                     <label class="col-lg-2 control-label">Assign To</label>
                      <div class="col-lg-4"> 
                      <select class="form-control" name="members" readonly="">
                     <!--   <?php if($allNames){?> -->
                        <option value="<?php echo $parkd[0]->uid ?>"><?php echo $parkd[0]->fullname?></option>
                        <!-- <?php foreach($allNames as $key => $value){?>
                        <option value="<?php echo $value->uid ?>"><?php echo $value->fullname; ?></option>
                        <?php } ?>
                       <?php } ?> -->
                      </select>
                    </div>
                  <div class="clearfix" style="height: 10px;clear: both;"></div>
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Parking Slot</label>
                       <div class="col-lg-4"> 
                     <select class="form-control" name="slot_no" required="slot_no">
                       <option value="<?php echo $selected_parking[0]->id ?>" selected><?php echo $selected_parking[0]->slot_no;?></option>
                       <?php if($edit_slot){?>
                        <?php foreach($edit_slot as $key => $value){?>
                        <option value="<?php echo $value->id;?>"><?php echo $value->slot_no;?></option>
                        <?php }?>
                       <?php }?>
                    </select>
                  </div>
                </div>
                       <div class="clearfix" style="height: 10px;clear: both;"></div>
                       <div class="form-group">
                                <label class="col-lg-3 control-label" for="commission from"></label>
                                <div class="col-lg-6">
                                      <button type="submit" id="submit_btn" class="btn btn-primary pull-left">Save</button>
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                      <a class="btn btn-primary" href="<?php echo  base_url().'back/parking'?>" type="button">Back</a> 
                                </div>
                            </div>
                                
                          </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<script>
  $( function() {
       $( "#transaction_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
</script>