<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Add Parking Slot
                                     <hr>
                                </h3>
                            </div>
                        </div>
                      <div class="box-body">
                         
                          <form id="payment_request" name="payment_request" method="post" action="<?php echo base_url();?>back/parking/insert_slot/">
                            <div class="form-group">
                           <label class="col-lg-2 control-label">Slot_Number </label>
                           <div class="col-lg-4">
                               <input type="text" class="form-control" name="slot_number" id="slot_number" value="<?php ?>" placeholder="Enter slot number" required="slot_number">
                          </div>
                     </div>
                  <div class="clearfix" style="height: 10px;clear: both;"></div>
                  <div class="form-group">
                           <label class="col-lg-2 control-label">Slot_For </label>
                           <div class="col-lg-4">
                               <input type="text" class="form-control" name="slot_for" id="slot_for" value="<?php ?>" placeholder="Enter slot for" required="slot_for">
                          </div>
                     </div>
                  <div class="clearfix" style="height: 10px;clear: both;"></div>
                      
 
                      <div class="clearfix" style="height: 10px;clear: both;"></div>
                  
                       <div class="clearfix" style="height: 10px;clear: both;"></div>
                   <div class="col-lg-offset-3">
                  <button type="submit" id="submit" class="btn btn-primary pull-left">Save</button>
                  </div>
                      <div class="form-group">
                          <div class="col-lg-offset-4">
                              <a class="btn btn-primary" href="<?php echo  base_url().'back/parking'?>" type="button">Back</a> 
                          </div>
                      </div>

                                
                          </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<script>
  $( function() {
       $( "#transaction_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
</script>