<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Parking List</h3>
                <?php if($this->session->userdata('role_id')==SOCIETY_SUPERUSER){?>
                <span style="float: right;margin-top: 10px;margin-right: 10px;"><button class="btn btn-primary add_btn">Assign Parking Slot</button></span>
                <span style="float: right;margin-top: 10px;margin-right: 10px;"><button class="btn btn-primary add_slot">Add Parking Slot</button></span>

                <?php } ?>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="mail_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">#</th>
                            <th>Id</th>
                            <th>Name</th>                          
                            <th>Parking Slot:</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>                                           

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
<script type="text/javascript">
 function doconfirm()
{
    job=confirm("Are you sure to delete permanently?");
    if(job!=true)
    {
        return false;
    }
}   
</script>



