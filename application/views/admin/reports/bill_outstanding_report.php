<section class="content">
    <div id="mail_template_wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <div class="row">
                    </div>
                    <div class="row">
                        <h3 class="box-title" style="margin-left:25px;">Members Outstanding List</h3>
                    </div>
                    <div class="box-header">
                        <div class="row">

                            <div class="col-md-2" style="">
                                <div style="position: static;" class="form-group">
                                    <label for="select-1">User List</label>
                                    <select class="form-control chosen" id="user_id" name="user_id">
                                        <option value="">All</option>
<?php if ($userdata) {?>
			                                        <?php foreach ($userdata as $key => $value) {?>
						                                        <option value="<?php echo $value->id?>"><?php echo $value->first_name." ".$value->last_name?></option>
		<?php }?>
			                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2" style="visibility: hidden;">test</label>
                                    <button type="submit" class="form-control btn btn-primary" id="generate_btn">Submit</button>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div style="" class="form-group">
                                    <label for="input-text-2" style="visibility: hidden;">test</label>
                                    <a href="<?php echo base_url().'back/bill_payment_report/statement'?>"><button type="button" class="form-control btn btn-warning" style="border-radius: 20px;">view transactions</button></a>
                                </div>
                            </div>
<?php if ($this->session->userdata('role_id') == SOCIETY_SUPERUSER) {?>
			                            <div class="col-md-3">
			                                <div style="" class="form-group">
			                                    <label for="input-text-2" style="visibility: hidden;">test</label>
			                                    <a href="<?php echo base_url().'back/email/mail_to_all'?>"><button type="button" class="form-control btn btn-danger" style="border-radius: 20px;"><i class="fa fa-send"></i>&nbsp;
			                                    Mail Remainder</button></a>
			                                </div>
			                            </div>
	<?php }?>

<?php if ($this->session->userdata('role_id') == SOCIETY_SUPERUSER) {?>                             <div class="col-md-3">
			                                <div style="" class="form-group">
			                                    <label for="input-text-2" style="visibility: hidden;">test</label>
			                                    <a href="<?php echo base_url().'back/sms/SmstoAll'?>"><button type="button" class="form-control btn btn-success" style="border-radius: 20px;"><i class="fa fa-envelope"></i>&nbsp;
			                                    SMS Remainder</button></a>
			                                </div>
			                            </div>
	<?php }?>
</div>


                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="mail_display_table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="20px">#</th>
                                    <th>Id</th>
                                    <th>Full Name</th>
                                    <th>Current outstanding</th>
                                    <th>wallet balance</th>
                                    <th>View Transactions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
    <script type="text/javascript">
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            var tconfig = {
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": base_url + "back/bill_payment_report/get_outstanding",
                    "type": "POST",
                    "data": "json",
                    data   : function (d) {
                        d.user_id    =  $("#user_id").val();
                    }
                },
                "iDisplayLength": 10,
                "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
//        "paginate": true,
"paging": true,
"searching": true,
"order": [[1, "desc"]],
"aoColumnDefs": [
{
    "targets": [1],
    "visible": false,
    "searchable": false

}
],

"fnRowCallback": function(nRow, aData, iDisplayIndex) {
    var info = $(this).DataTable().page.info();
    $("td:first", nRow).html(info.start + iDisplayIndex + 1);
    if (aData[8] == 'Approved' || aData[8] == 'Rejected')
    {
        $("td:eq(8)", nRow).html('<a href='+base_url+'back/bill_payment/bill_payment_view/'+aData[1]+' class="views_wallet_trans margin"><i class="glyphicon glyphicon-eye-open"></i></a>');
    }
    return nRow;
},
};

var oTable = $('#mail_display_table').dataTable(tconfig);
$(document).off('click', '#generate_btn').on('click', '#generate_btn', function (e) {
    oTable.fnDraw();
});
});
</script>

