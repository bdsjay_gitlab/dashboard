<section class="content">
  <div class="row">
    <form role="form" method="post" action="<?= base_url() ?>back/sms/sendSms" id="sms_form">
      <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Send SMS</h3>
            </div>
          <div class="box-body">
            <?php $role = $this->session->userdata('role_id'); 
            if($role == SUPERADMIN){?>
             <div class="form-group row">
                <label class="col-sm-3 col-form-label col-sm-offset-1" for="society_name">Select Society *</label>
                <div class="col-sm-5">
                  <select name="society_id" class="form-control" id="society_id">
                    <option value="">Society Name</option>
                    <option value="all">All Society</option>
                    <?php  foreach($society as $society) {?>
                    <option value="<?= $society->id; ?>"><?= $society->name; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
                <?php } ?>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label col-sm-offset-1" for="reason">SMS Notification For *</label>
                <div class="col-sm-5">
                  <select name="reason" class="form-control" id="reason">
                   <option value="">Select Notification Type</option>
<!--                    <option value="bp">Bill Payment</option>
                   <option value="n">Notices</option> -->
                   <option value="m">Event</option>
                   <option value="c">Complaint</option>
                   <option value="bpr">Bill Payment Remainer</option>
                   <option value="nr">Notices Remainder</option>                     
                   <option value="mr">Event Remainder</option>
                   <option value="cr">Complaint Remainder</option>
                   <option value="o">Other</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 col-form-label col-sm-offset-1" for="group">Message To *</label>
                <div class="col-sm-5">
                  <select name="group" class="form-control" id="group">
                    <option value="">Select Contact</option>
                   <?php if($role == SUPERADMIN) {?>
                   <option value="all">All</option>
                   <option value="2">Society Superuser</option>
                   <option value="3">Admin</option>
                   <?php } if($role == SOCIETY_SUPERUSER){?>
                   <option value="all">all</option>
                   <option value="3">Admin</option>
                   <option value="auth">Authority</option>
                   <?php } else {?>
                   <?php }?>
                   <option value="4">Member</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="description">Users to send *</label>
                  <div class="col-sm-7">
                    <select name="sendto[]" id="sendto" class="form-control sendto" multiple required style="margin-bottom: 10px;width:45%">
                    </select>
                     <button type="button" class="btn btn-xs btn-success chosen-toggle select">Select all</button>&nbsp;&nbsp;
                      <button type="button" class="btn btn-xs btn-danger chosen-toggle deselect">Deselect all</button>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-sm-3 col-form-label col-sm-offset-1" for="description">Message *</label>
                  <div class="col-sm-7">
                   <textarea class="textarea" name="msg" placeholder="Message To send" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                  </div>
               </div>
               <div class="form-group row">
                <label class="col-sm-3 col-form-label col-sm-offset-1" for="description"></label>
                  <div class="col-sm-7">
                    <button type="submit" class="btn btn-primary" id="submit">Send Sms</button>&nbsp;&nbsp;
                    <button type="reset" value="reset" class="btn btn-warning">Clear</button>
                  </div>
               </div>
            </div>
          </div>
        </div>
    </form>
  </div>
</section>  
<script type="text/javascript">
  $(document).ready(function(){

    $('.chosen-toggle').each(function(index) {
    console.log($(this).parent().find('option').text());
    $(this).on('click', function(){
      $(this).parent().find('option').prop('selected', $(this).hasClass('select')).parent().trigger('chosen:updated');
    });
});

    $('#group').on('change',function(){
      var society_id = $('#society_id').val();
      var reason     = $('#reason').val();
      var group      = $('#group').val();
      console.log(society_id+'-'+reason+' '+group);
       $('.sendto').empty();
        $.ajax({
          url:'<?= base_url(); ?>/back/sms/fetchUsers',
          type:'POST',
          data:{society_id:society_id,reason:reason,group:group},
          success:function(response){
            $('.sendto').append(response);
          }
        });
    });

     $('#society_id').on('change',function(){
      var society_id = $('#society_id').val();
      var reason     = $('#reason').val();
      var group      = $('#group').val();
      console.log(society_id+'-'+reason+' '+group);
       $('.sendto').empty();
        $.ajax({
          url:'<?= base_url(); ?>/back/sms/fetchUsers',
          type:'POST',
          data:{society_id:society_id,reason:reason,group:group},
          success:function(response){
            $('.sendto').append(response);
          }
        });
    });
    
  });
</script>