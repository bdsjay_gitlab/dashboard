<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                           <div class="col-lg-7"> 
                                <h3 style="text-align: center;">
                                    Update House Details
                                     <hr>
                                </h3>
                            </div>
                        </div>
                      <div class="box-body">
                         
                  <form id="payment_request" name="payment_request" method="post" action="<?php echo base_url();?>back/societycharges/update/">
                    <div class="form-group">
                     
                     <label class="col-lg-2 control-label">House Type</label>
                      <div class="col-lg-4"> 
                      <input type="text" class="form-control" name="house_type" id="house_type" value="<?php echo $house_type[0]['house_type'];?>" readonly>
                    </div>
                  <div class="clearfix" style="height: 10px;clear: both;"></div>
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Maintenance Amount</label>
                       <div class="col-lg-4"> 
                    <input type="text" class="form-control" name="maintenence_amount" id="maintenence_amount" value="<?php echo $house_type[0]['maintenance_charges'];?>">
                  </div>
                </div>
                 <input type="hidden" name="id" value="<?php echo $house_type[0]['id'];?>">
                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Lease Amount</label>
                       <div class="col-lg-4"> 
                    <input type="text" class="form-control" name="lease_amount" id="lease_amount" value="<?php echo $house_type[0]['lease_amt'];?>">
                  </div>
                </div>
                <?php if($flag == '0'){?>
                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                   <div class="form-group">
                      <label class="col-lg-2 control-label">Want Bifurcation's</label>
                       <div class="col-lg-4"> 
                    <div class="form-check form-check-inline">
                          <input class="form-check-input" type="checkbox" id="parkcheckbox1" value="house_type" name="house_type_check">
                          <label class="form-check-label" for="inlineCheckbox1">
                        </div>
                  </div>
                </div>
              <?php }?>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                   <div class="form-group" id="house_div">
                      <label class="col-lg-2 control-label">Amt Bifurcation</label>
                       <div class="col-sm-10">
                      <div class="table-responsive" id="cheque_detail">
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr class="bg-light-blue-active">
                                    <th>Building Repair</th>
                                    <th>Lift Repairs</th>
                                    <th>Water Charges</th>
                                    <th>Property Tax</th>
                                    <th>Insurance Charges</th>
                                    <th>Service Charges</th>
                                    <th>Other Charges</th>
                                    <th>Action</th><!--
                                    <th>9</th>    -->
                                </tr>
                            </thead>
                       <tbody>
                                <tr data-row_no="1">
                                    <td>
                                         <input type="text" name="Building_Repair" id="Building_Repair" class="sd" placeholder="House Type" style="width: 8em" value="<?php echo isset($bifurcation_data['Building_Repair']) ? $bifurcation_data['Building_Repair'] : '0.00' ?>" required>
                                    </td>
                                    <td>
                                        <input type="text" name="Lift_Repairs" id="Lift_Repairs" class="main_amt sd" placeholder="Maintenance Amt" style="width:8em" min="0" value="<?php echo isset($bifurcation_data['Lift_Repairs']) ? $bifurcation_data['Lift_Repairs'] : '0.00' ?>" required>
                                    </td>
                                    <td>
                                        <input type="text" name="Water_Charges" id="Water_Charges" class="lease_amt sd" placeholder="Lease Amt" style="width: 8em" value="<?php echo isset($bifurcation_data['Water_Charges']) ? $bifurcation_data['Water_Charges'] : '0.00' ?>" min="0" required>
                                    </td>
                                    <td>
                                         <input type="text" name="Property Tax" id="Property_Tax" class="sd" placeholder="House Type" style="width: 8em" value="<?php echo isset($bifurcation_data['Property_Tax']) ? $bifurcation_data['Property_Tax'] : '0.00' ?>" required>
                                    </td>
                                    <td>
                                        <input type="text" name="Insurance_Charges" id="Insurance_Charges" class="main_amt sd" placeholder="Maintenance Amt" style="width:8em" min="0" value="<?php echo isset($bifurcation_data['Insurance_Charges']) ? $bifurcation_data['Insurance_Charges'] : '0.00' ?>" required>
                                    </td>
                                    <td>
                                        <input type="text" name="Service_Charges" id="Service_Charges" class="lease_amt sd" placeholder="Lease Amt" style="width: 8em" min="0" value="<?php echo isset($bifurcation_data['Service_Charges']) ? $bifurcation_data['Service_Charges'] : '0.00' ?>" required>
                                    </td>
                                    <td>
                                         <input type="text" name="Other_Charges" id="Other_Charges" class="sd" placeholder="House Type" style="width: 8em" value="<?php echo isset($bifurcation_data['Other_Charges']) ? $bifurcation_data['Other_Charges'] : '0.00' ?>" required>
                                    </td>
                                    <td>
                                      <a href="<?php echo  base_url().'back/societycharges/delete_bifurcation/'.$id?>">
                                          <input type="button" name="add" value="Remove" class="btn btn-danger" onclick="return confirm('Are you sure want to remove this Bifurcation ?')"/>
                                      </a>
                                    </td><!--
                                    <td>
                                        <input type="text" name="nine" id="nine" class="lease_amt sd" placeholder="Lease Amt" style="width: 8em" min="0" value="0.00" required>
                                    </td> -->
                                </tr>
                            </tbody>
                        </table>
                     </div>
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label"></label>
                       <div class="col-lg-4"> 
                        <center>
                            <button type="submit" id="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp; 
                              <a class="btn btn-danger" href="<?php echo  base_url().'back/societycharges'?>" type="button">Back</a>
                              </center> 
                          
                         </div>
                  </div>
                </div>
                       <div class="clearfix" style="height: 10px;clear: both;"></div>
                <!--    <div class="col-lg-offset-3">
                  <button type="submit" id="submit" class="btn btn-primary pull-left">Save</button>
                  </div>
                      <div class="form-group">
                          <div class="col-lg-offset-4">
                              <a class="btn btn-primary" href="<?php echo  base_url().'back/parking'?>" type="button">Back</a> 
                          </div>
                      </div> -->

                                
                          </form>
                    <!--</div> /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
<script>
  $( function() {
       $( "#transaction_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
  } );
</script>
<script type="text/javascript">

  if("<?php echo $flag == '1'?>")
  {
    $('#parkcheckbox1').attr('checked','checked');
    $("#house_div").show();
  }
  else
  {
    $("#house_div").hide();
  }
$(function () {
        $("#parkcheckbox1").click(function () {
            if ($(this).is(":checked")) {
                 $("#house_div").show();
            } else {
                 $("#house_div").hide();
            }
        });
    });
</script>
<script type="text/javascript">
   $("form").submit(function(e){   
       if($("#parkcheckbox1").prop("checked") == true)
       {
         var first = $('#Building_Repair').val();
         var second = $('#Lift_Repairs').val();
         var third = $('#Water_Charges').val();
         var four = $('#Lift_Repairs').val();
         var five = $('#Insurance_Charges').val();
         var six = $('#Service_Charges').val();
         var seven = $('#Other_Charges').val();
       /*  var eight = $('#eight').val();
         var nine = $('#nine').val();*/
         
         var final = parseFloat(first)+parseFloat(second)+parseFloat(third)+parseFloat(four)+parseFloat(five)+parseFloat(six)+parseFloat(seven)/*+parseFloat(eight)+parseFloat(nine)*/; 
      
         var maintenance = $('#maintenence_amount').val();

         if(final > maintenance)
         {
            alert('Your Bifurcation amount greater than Maintenance Amount');
            e.preventDefault();
         }
       }
    });
</script>