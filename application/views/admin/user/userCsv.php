<section class="content">
    <div class="row">
    <div class="col-md-12">
     <a href="javascript:window.history.go(-1);" class="btn btn-danger pull-right"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
   </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-sm-12 col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Import Users</h3>
        </div><br>
      <form action="<?php echo base_url(); ?>back/registration/userCsv" method="post" name="upload_excel" enctype="multipart/form-data">
        <div class="form-group  text-center">
           <input type="file" class="center-block" name="file" id="file" required><br>
        </div>
        <div class="form-group  text-center">
           <button class="btn btn-success" type="submit" id="submit" name="user">Import Users</button>
        </div>
      </form>
      <div class="form-group  text-center">
      <a href="<?php echo base_url(); ?>upload/sample/users.csv"> Sample csv file </a>
      </div><br>
    </div>
  </div>
    <div class="col-sm-12 col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Help</h3><h1></h1>
          <br>
          <ul type="square">
            <li>Download the sample CSV file.</li>
            <li>Enter the data in the downloaded file.</li>
            <li>Keep the data same as it's in the website <br>e.g. society name in CSV should be exact same as website, etc.</li>
            <li>Only Csv Format Allowed.</li>
          </ul>
        </div><br>
      
      <div style="width:80%; margin:0 auto;" align="center">
        <table id="t01">
          <tr>
            <td></td>
          </tr>
          <?php
          if(isset($view_data) && is_array($view_data) && count($view_data)): $i=1;
          foreach ($view_data as $key => $data) { 
          ?>
          <tr>
            <td><?php echo $data['name'] ?></td>
            <td><?php echo $data['email'] ?></td>
            <td><?php echo $data['created_date'] ?></td>
          </tr>
          <?php } endif; ?>
        </table>
      </div>
  </div>
</div>
</div>
</section>
