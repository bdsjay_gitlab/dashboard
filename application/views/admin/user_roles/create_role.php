 <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;Create New User Role
             <!--<small>advanced tables</small>-->
        </h3s>
       
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php
                            $attributes = array("method" => "POST", "id" => "group_create_form", "class" => "group_create_form");
                            echo  form_open(site_url().'/admin/user_role/save_role', $attributes);
                       ?>
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Role Name</label>
                            <div class="col-lg-6">
                                <input name="group_id" type="hidden" id="group_id" class="form-control" value="">
                                <input name="group_name" type="text" id="group_name" class="form-control" value="" required="">
                               
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Homepage: </label>
                            <div class="col-lg-6">
                                <select name="homepage" class="form-control" id="front_back_access">
                                    <option value=''>Select Homepage</option>
                                    <option value="home"> Home</option>
                                    <option value='dashboard'>Dashboard</option>
                                </select>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Access: </label>
                            <div class="col-lg-6">
                                <select name="access" class="form-control" id="front_back_access">
                                    <option value=''>Select Access</option>
                                    <option value='front'>Front</option>
                                    <option value="back"> Back</option>
                                    <option value="both"> Both</option>
                                </select>
                            </div>
                        </div>                        
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">

                                <button class="btn btn-primary" id="save_group_data" name="save_group_data" type="submit">Save</button> 
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div>
                        </div>

                         </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->


<script>

    $(document).ready(function() {

    $(document).off('click', '.back').on('click', '.back', function(e)
    {

        window.location.href = base_url + 'back/user_role/';
    });
    });
</script>

@endpush
@stop