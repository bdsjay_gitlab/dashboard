<script src="<?php echo base_url()?>assets/common_js/complaint.js"></script>

<section class="content">
    <div id="mail_template_wrapper">
        <?php
        $this->load->view(COMPLAINT_TEMPLATE_AJAX);
        ?>
    </div>

    <?php $this->load->view(COMPLAINT_TEMPLATE_POPUP); ?>
</section>
