<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BDS Serv</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">
 <link rel="shortcut icon" href="<?= base_url()?>assets/images/favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?= base_url()?>assets/images/favicon.ico" type="image/x-icon">
</head>
 
<body>
<h3 style="margin-left:15px;">Encrypt Val</h3>

<div class="" style="margin-left:15px;">
<form id="encrypt_form" name="encrypt_form">
<textarea name="inp" class="inp" cols="60"></textarea>
<input type="button" value="Encypt" class="submit_btn" id="encrypt">
</form>
<textarea class="enc_retval" readonly cols="60" val=""></textarea>
</div>

<p></p>
<hr>

<h3 style="margin-left:15px;">Decrypt Val</h3>

<div class="" style="margin-left:15px;">
<form id="decrypt_form" name="decrypt_form">
<textarea name="inp" class="inp" cols="60"></textarea>
<input type="button" value="Decrypt" class="submit_btn" id="decrypt">
</form>
<textarea class="dec_retval" readonly cols="60"></textarea>
</div>

<script type="text/javascript">
  base_url = "<?php echo base_url();?>"
  console.log($('#ret').closest("form").find(".retval").val());
</script>
<script>
$(document).on("click", ".submit_btn", function() {
var curr_obj = $(this);
var inp_key = curr_obj.attr("id");
var inp_val = curr_obj.closest("form").find(".inp").val();
if(inp_val)
{
      $.ajax({
    method: "POST",
    url: base_url+"tools/get_value",
    data: {inp_key: inp_key, inp_val:inp_val }
    }).done(function(data) {    

      if(data.msg_type == "success")
      {
        if(inp_key == "encrypt" || inp_key == "decrypt")
        {
          if(inp_key == "encrypt")
          {
             $('.enc_retval').val(data.msg);
          }
          else
          {
             $('.dec_retval').val(data.msg);
          }
        }       
        else
        {
          alert("error");
        } 
      }
      else
      {
        alert("error");
      }  
    });
}
else
{
 alert('please provide input');
}

});
</script>
</body>
</html>
