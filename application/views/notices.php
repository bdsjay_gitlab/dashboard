<script src="<?php echo base_url()?>assets/plugins/ckeditor/ckeditor.js"></script>

<script src="<?php echo base_url()?>assets/common_js/mail_template.js"></script>

<section class="content">
    <div id="mail_template_wrapper">
        <?php
        $this->load->view(NOTICE_TEMPLATE_AJAX);
        ?>
    </div>

    <?php $this->load->view(NOTICE_TEMPLATE_POPUP); ?>
</section>
<!-- CK Editor -->

<script type="text/javascript">
    $(function() {
        
        CKEDITOR.replace('mail_body_class',{
            enterMode : CKEDITOR.ENTER_BR,
            entities : false,
            basicEntities : false,
            entities_greek : false,
            entities_latin : false, 
            htmlDecodeOutput : false
        });
        
    });
</script>