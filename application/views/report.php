<section class="content">
  <div id="wallet_wrapper">
    <div class="row">
        <div class="col-xs-12">
            <form action="http://180.92.175.107/rokad/index.php/#" method="POST" id="userrpt" name="userrpt" onsubmit="return false;" accept-charset="utf-8">
                <div class="box" style="padding:10px;">
                    <div class="row">
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label>From Date</label>
                                <input type="text" class="form-control" id="from_date" name="from_date" placeholder="19-02-2018 " >
                            </div>
                        </div>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label>To Date</label>
                                <input type="hidden" name="def_to_date" id="def_to_date" value="2018-02-19 ">
                                <input type="hidden" name="def_from_date" id="def_from_date" value="2018-02-19">
                                <input type="text" class="form-control" id="to_date" name="to_date"  placeholder="19-02-2018" >
                            </div>
                        </div>   
                        <div style="display: none;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group"> 
                                <label for="select-1">User Type</label>
                                <select class="form-control" id="user_type" name="user_type">
                                    <option value="all">All</option>                                    
                                    <option value='3' >Company</option>                                    
                                    <option value='4' >Regional Distributor</option>                                    
                                    <option value='5' >Divisional Distributor</option>                                    
                                    <option value='6' >Executive</option>                                  
                                    <option value="7">Sale Agent</option>
                                </select>
                            </div>
                        </div>
                    </div>  
               <div class="row"> 
                   <div style="display: block;" class="col-md-1 display-no">
                       <div style="position: static;" class="form-group">
                           <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit">Search</button>
                       </div>
                   </div>
                   <div style="display: block;" class="col-md-1 display-no">
                       <div style="position: static;" class="form-group">
                           <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                       </div>
                   </div>
                   <div style="display: block;" class="col-md-2 display-no">
                       <div style="position: static;" class="form-group">
                           <button  class="btn btn-primary" id="extoexcel" type="button">
                       <span class="glyphicon glyphicon-export"></span>Export To Excel</button>  
                       </div>
                   </div>
               </div>
            </div>
        </div>
               <div class="row">
                  <div class="col-xs-12">
                     <div class="box">
                         <div class="box-body" id="table_show">
                             <table id="op_rept" name="op_rept" class="table table-striped">
                                 <thead><tr>
                                        <th>Sr.No.</th>
                                        <th>id</th>
                                        <th>username</th>
                                        <th>User Type</th>
                                        <th>State</th>
                                        <th>City</th>
                                        <th>Mobile No</th>
                                        <th>Email Id</th> 
                                  <tbody>
                                </tbody>
                          </table>
                      </div>
                 </div>
             </div>
         </div>
       <div class="clearfix" style="height: 10px;clear: both;"></div>   
    </form>  </div>   
</div>
       
</section>

<script>
var tkt_data;
$(document).ready(function() {
  var ajax = "http://localhost/sms/admin/report_list";      
  var tconfig = {
    "processing": true,
    "serverSide": true,
    "searching": false,
    "iDisplayLength": 25,
    "aLengthMenu": [[5, 10, 25,50, -1], [5, 10, 25,50, "All"]],
    "order": [[2, "desc"]],
    "ajax": {
      "url": ajax,
      "type": "POST",
      "data": "json",
      data   : function (d) {
//        d.from_date   =  $("#from_date").val();
//        d.to_date     =  $("#to_date").val();
                        var date = new Date();
                        var from_date;
                        var to_date;
                        if ($("#from_date").val() == '') {
                        from_date = $("#def_from_date").val();
                        } else {
                        from_date = $("#from_date").val();
                        }
                        if ($("#to_date").val() == '') {
                        to_date = $("#def_to_date").val();
                        } else {
                        to_date = $("#to_date").val();
                        }
        d.from_date = from_date;
        d.to_date = to_date;
       }
    },
    fnDrawCallback: function(data)
    {

    },
    "columnDefs": [
      {
        "searchable": false
      }
    ],
    "scrollX": true,
    "bFilter": false,
    "bPaginate": true,
    "bRetrieve": true,
    "aoColumnDefs": [
      {
        "bSortable": false,  
        "aTargets": [0]
      },
      {
        "bSortable": false,  
        "aTargets": [3]
      },
      {
        "bSortable": false,  
        "aTargets": [4]
      },
      {
        "bSortable": false,  
        "aTargets": [5]
      },
      {
        "bSortable": false,  
        "aTargets": [6]
      },
      {
        "bSortable": false,  
        "aTargets": [7]
      },
      {
        "bSortable": false,  
        "aTargets": [1]
      },
    ],
    "oLanguage": {
                  "sProcessing": '<img src="http://180.92.175.107/rokad/assets/adminlte/back/img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
                },
  };
  tkt_data= $('#op_rept').dataTable(tconfig);     
  $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function (e) {
    tkt_data.fnDraw();       
  });          

    $( "#from_date" ).datepicker({
        
    });
    $( "#to_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate:0
    });
  });

</script>
<script type="text/javascript">
/*        var detail = {};
        var div = '';
        var str = "";
        var form = '';
        detail['w_id'] = 'M2rTEScfNDf1G2JRCd218DkhaUDWz7zcYA ';
        var ajax_url = '<?php echo base_url() ?>home/logout';

        get_data(ajax_url, form, div, detail, function (response)
        {
            console.log(response);
        }, '', false);*/
</script>