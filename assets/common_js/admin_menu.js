$(document).ready(function() {
    $.validator.setDefaults({
        ignore: ":hidden:not(select)"
    });
    
    $("#menu_form").validate({
        rules: {
            parent: {
                required: true     
            },
            menu_name: {
                required: true
            },
            menu_link: {
                required: true
            }
        }
    });

    var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "back/menu/get_list",
            "type": "POST",
            "data": "json",
            data: function(d){
            }
        },
        // "sAjaxSource": BASE_URL+'/data_table_eg/get_list1',
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        //        "paginate": true,
        "paging": true,
        "searching": true,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    var active_btn = 'fa fa-square-o';
                    if (aData[5] == 'Y')
                    {
                        $("td:eq(5)", nRow).html("<button type='button' class='btn btn-success btn-xs' style='cursor:auto'>Enable</button>");
                        active_btn = 'fa fa-check-square-o';
                    }
                    else
                    {
                        $("td:eq(5)", nRow).html("<button type='button' class='btn btn-danger btn-xs' style='cursor:auto'>Disable</button>");
                    }

                     var btn = '';
                     btn +='<a title="Edit" href="" class="edit_btn margin" ref="'+ aData[0] +'"> <i  class="glyphicon glyphicon-pencil"></i> </a>'
                     btn += '<a title="Status" class="del_btn" ref="' + aData[0] + '" data-ref="'+ aData[5] +'" style="cursor:pointer"><i class="' + active_btn + '"></i></a>';
                     $("td:last", nRow).html(btn);
        }
    };

    var oTable = $('#menu_display_table').dataTable(tconfig);
        
    $(document).off('click', '.add_btn').on('click', '.add_btn', function () {
        //$('#menu_form').reset_form();
        $('#add_edit_popup').modal('show');
    });


    $(document).off('click', '#submit_btn').on('click', '#submit_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');

        var detail = {};
        var div = "";
        var ajax_url = base_url+'back/menu/savemenu';
        var form = 'menu_form';
        
        if($('#menu_form').valid()) 
        {
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    //$('#menu_form').reset_form();
                    $('.popup').modal('hide');
                    if(response.msg_type == "success")
                    {
                        showLoader(response.msg_type,response.msg);
                        setTimeout(location.reload(), 5000);
                    }
                }
                else
                {
                    if(response.msg_type == "error")
                    {
                        showLoader(response.msg_type,response.msg); 
                    }
                    else
                    {
                        alert(response.msg);
                    }     
                }
            }, '', false);
            oTable.fnDraw();
        }
    });

    $(document).off('click', '.edit_btn').on('click', '.edit_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
            
        var detail = {};
        var div = "";
        var ajax_url = base_url+'back/menu/edit_menu';
        var form = '';

        detail['id'] = id;

        get_data(ajax_url, form, div, detail, function (response)
        {
            if (response.flag == '@#success#@')
            {
                menu_data = response.menu_data;
                
                $("#menu_form #id").val(menu_data.id);
                $("#menu_form #menu_parent").val(menu_data.parent);
                //to update chosen select
                $('select').trigger("chosen:updated");
                $("#menu_form #menu_name").val(menu_data.display_name);
                $("#menu_form #menu_link").val(menu_data.link);
                $("#menu_form #menu_class").val(menu_data.class);
                $('#add_edit_popup').modal('show');

            // $('#submit_btn').removeClass('disable');
            }
            else
            {
                alert(response.msg);  
            }
        }, '', false);
    });

    $(document).off('click', '.del_btn').on('click', '.del_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var yn = $(this).attr('data-ref');
        if (yn == "N")
        {
            var postyn = "Y"
            var act = "enable";
        }
        else if (yn == "Y")
        {
            var postyn = "N"
            var act = "disable";
        }
            
        var detail = {};
        var div = "";
        var ajax_url = base_url+'back/menu/delete_menu';
        var form = '';

        detail['id'] = id;
        detail['postyn'] = postyn;
            
        var yesno = confirm("Are you sure you want to "+ act +" this data");
        if (yesno)
        {
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    alert(response.msg);
                    location.reload();
                }
                else
                {
                    alert(response.msg);
                }
            }, '', false);
        }
    });

       
});