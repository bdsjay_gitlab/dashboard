/*
 function    : get_data
 description : function will ajax response
 
 @param         ajax_url        URL where call ajax call is to be made 
 @param         form            id of 'form' from which data is to be serialized and submitted
 @param         form_div        id of 'form' where ajax response is loaded
 */
function get_data(ajax_url, form, form_div, extra_parameters, callback_function, callback_function_on_complete, async_flag) {
    if (typeof (async_flag) == 'undefined') {
        async_flag = true;
    }

    var  _opt = {
        type: "POST",
        url: ajax_url,
        dataType: 'json',
        cache: false,
        async: async_flag,
        data: '',

    };

    var data = "";

    if (form != "") {
        data = $("#" + form).serialize();

        var formType = $('#'+form).attr('enctype');

        if(typeof(formType) != 'undefined' && formType != false && formType == 'multipart/form-data')
        {
            data = new FormData( document.getElementById(form));
            _opt.processData = false;
            _opt.contentType = false;

        }

    }

    if (extra_parameters != undefined) {
        for (var key in extra_parameters)
        {
            data = data + "&" + key + "=" + extra_parameters[key];
        }
    }
    _opt.data       = data;
    _opt.success    = function(response) {
        var json_data_object = response;
        var success_flag = response['flag'];
        if ((success_flag == '@#success#@') || (success_flag == '@#sorry#@') || (success_flag == '@#error#@')) {
            var view = response['view'];
            if (jQuery.isArray(form_div)) {
                for (var key in form_div) {
                    if (response[key] != undefined) {
                        if (response[key] == "0") {
                            $("#" + form_div[key]).hide();
                        }
                        else {
                            $("#" + form_div[key]).show();
                            $("#" + form_div[key]).html(response[key]);
                        }
                    }
                }
            }
            else {
                $("#" + form_div).html(view);
            }
        }
        else if (response.redirect_url) {
            window.location.href = response['redirect_url'];
        } else if (response.redirect_html) {
            $(response.redirect_target_element).html(response.redirect_html);
        }

        if (typeof callback_function == 'function') { // make sure the callback is a function
            callback_function(response); // brings the scope to the callback
        }

    };

    _opt.complete = function() {
        if (typeof callback_function_on_complete == 'function') {
            callback_function_on_complete();
        }

    //reset_message_timer();
    };

    $.ajax( _opt );
}
/*function bootstrapGrowl(msg, msg_type, msg_config)
{
    var offset_from = 'top';
    var offset_amount = 10;

    if (typeof (msg_type) == 'undefined')
    {
        msg_type = 'info';
    }

    if (typeof (msg_config) == 'undefined')
    {
        msg_config = {};
    }
    else if(typeof (msg_config.offset) != 'undefined' && msg_config.offset != null)
    {
        offset_from = (msg_config.offset.from != undefined && msg_config.offset.from != "") ? msg_config.offset.from : "top";
        offset_amount = (msg_config.offset.amount != undefined && msg_config.offset.amount != "") ? msg_config.offset.amount : 10;
    }

    var config = {
                  ele: 'body', // which element to append to
                  type: msg_type, // (null, 'info', 'danger', 'success')
                  offset: {from: offset_from, amount: offset_amount}, // 'top', or 'bottom'
                  align: 'right', // ('left', 'right', or 'center')
                  width: 350, // (integer, or 'auto')
                  delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                  allow_dismiss: true, // If true then will display a cross to close the popup.
                  stackup_spacing: 10 // spacing between consecutively stacked growls.                
                };

    var fConfig = $.extend({}, config, msg_config);

    $.bootstrapGrowl(msg, fConfig);
}*/
