﻿$(function(){
    
    $("#user_form").validate({
         //ignore: "none:not(select)",
        rules: {
            first_name: {
                required: true,
                //lettersonly: true
            },
            last_name: {
                required: true,
                //lettersonly: true
            },
            email: {
                required: true,
                email: true
            },
            mobile: {
                required: true,
                minlength: 10,
                maxlength: 10,
                number: true,
                valid_mobile_no: true,
                mobileno_start_with:true
            },
            birth_date:{
                required: true,
             },
             username: {
                required:true,
                minlength:8,
                maxlength:16,
                username_exists: true
            },
            password: {
                required: true
            },
            total_family_member:{
                required: true,
                number: true
            }
            
        },
        errorPlacement: function (error, element) {
                   
            if(element.attr("name") === "society_id")
            {
                error.appendTo(element.parent("div") );
            }
            else if(element.attr("name") === "house_id")
            {
                error.appendTo(element.parent("div") );
            }
            else {
                error.insertAfter(element);
            }
                
        },
        messages:
                {
                    
                    first_name: {
                        required: "Please Enter First Name",
                        lettersonly: "Please Enter Valid First Name",
                    },
                    last_name: {
                        required: "Please Enter Last Name",
                        lettersonly: "Please Enter Valid Last Name",
                    },
                    email: {
                        required: "Please Enter Email Address",
                        email: "Please Valid Email Address",
                    },     
                    birth_date: {
                        required: "Please Select The DOB (DD-MM-YYYY)",
                    },
                    mobile: {
                        required: "Please Enter Mobile No",
                        number: "Only numbers allowed",
                        minlength: "Please Enter 10 Digit Mobile No",
                        maxlength: "Please Enter 10 Digit Mobile No",
                        valid_mobile_no: "Please Enter Valid Mobile No",
                        mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
                    },
                    birth_date:{
                         required: "Please Enter Date Of Birth"
                    },
                    username:{
                          required:"Please Enter Username",
                          username_exists:"Username is Already Exist",
                          minlength:"Minimum 8 Character",
                          maxlength:"Max 16 Character Allowed"
                    },
                    password:{
                            required:"Please Enter Password",
                            minlength:"Minimum 8 Character",
                            maxlength:"Max 16 Character Allowed",
                    },
                    total_family_member:{
                          required: "Please Enter Total Family Member",
                          number  : "Only Number Allowed",
                          min:"Minimum 1 digit",
                          maxlength:"Max 3 Digit"
                    }
                 }
        });     
    $.validator.addMethod("valid_mobile_no", function (value, element) {
        return (value == "0000000000" || value == "9999999999") ? false : true;
    }, 'Please enter valid mobile number');
    
    $.validator.addMethod("mobileno_start_with", function (value, element) {
            var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Mobile No. should start with 7 or 8 or 9 ');

		$('#society_form').validate({
		rules:{
			society_name:{
				required:true,
				maxlength:80
			},
			email: {
                required: true,
                email: true
            },
            mobile: {
                required: true,
                minlength: 10,
                maxlength: 10,
                number: true,
                valid_mobile_no: true,
                mobileno_start_with:true
            },
			state: {
				required :true
			},
			city:{
				required:true
			},
			address:{
				required:true,
				maxlength:150
			},
			pincode:{
				required:true,
				number:true,
				minlength:6,
				maxlength:6			
			},
			no_of_house:{
				required:true,
				number:true,
				minlength:1,
				maxlength:5
			},
			username:{
				required:true,
				minlength:8,
				maxlength:16,
				username_exists: true

			},
			password:{
				required:true,
				minlength:8,
				maxlength:16
			}
		},
		messages:{
			society_name:{
				required: "Please Enter Society Name",
				maxlength:"Max 80 Character Allowed"
			},
			email: 
			{
                required: "Please Enter Email Address",
                email_exists:"Email id already registered with us"
            },
            mobile: 
            {
                required: "Please Enter Mobile No",
                number: "Only numbers allowed",
                minlength: "Please Enter 10 Digit Mobile No",
                maxlength: "Please Enter 10 Digit Mobile No",
                valid_mobile_no: "Please Enter Valid Mobile No",
                mobileno_exists: 'Mobile No Already Registered with us',
                mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
            },
			state:{
				required: "Please Select A State"
			},
			city:{
				required: "Please Select A City"
			},
			address:{
				required: "Please Enter Address",
				maxlength:"Address Not More Than 150 Character Allowed",
			},
			pincode:{
			   required: "Please Enter Pincode",
			   number: "Only Number Allowed",
			   minlength: "Miimum 6 Digit Pincode",
			   maxlength: "Max 6 Digit Pincode"
			},
			no_of_house:{
				required:"Please Enter No Of House In The Society",
				number:"Only Number Allowed",
				minlength:"No Of House Can't be 0 or less",
				maxlength:"Max 5 digit Allowed"
			},
			username:{
				required:"Please Enter Username",
				minlength:"Minimum 8 Character",
				maxlength:"Max 16 Character Allowed"
			},
			password:{
				required:"Please Enter Password",
				minlength:"Minimum 8 Character",
				maxlength:"Max 16 Character Allowed"
			}
		}
	});
	$.validator.addMethod("valid_mobile_no", function (value, element) {
        return (value == "0000000000" || value == "9999999999") ? false : true;
    }, 'Please enter valid mobile number');
    
    $.validator.addMethod("mobileno_start_with", function (value, element) {
            var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Mobile No. should start with 7 or 8 or 9 ');
 
	$('#house_form').validate({
		rules:{
			society_name:{
				required:true
			},
			building:{
				minlength:1,
				maxlength:25,
				required:true
			},
			wing: {
				lettersonly:true,
				minlength:1,
				maxlength:1
			},
			block: {
				required :true,
				alphanumeric: true,
				lettersonly:false,
	            minlength: 1,
	            maxlength:25
			},
			house_type:{
				required:true
			},
			details:{
				minlength:2,
				maxlength:50,
			}
		},
		messages:{
			society_name:{
				required:"Please Select A Society"
			},
			building:{
				minlength:"Minimum One letters or number Allowed",
				maxlength:"Max 25 Letters Or No. Allowed",
				 required: "Please Enter Society Name"
			},
			wing: {
				lettersonly:"Only Letters Allowed",
				minlength:"Minimum One Characterd Allowed",
				maxlength:"Only One Characterd Allowed"
			},
			block:{
				required: "Please Enter Block/Flat No",
				alphanumeric:"Only alphanumeric Allowed",
				lettersonly:"Only Letter not Allowed",
				minlength:"Min 1 Character Allowed",
				maxlength:"Max 25 Character Allowed"
			},
			house_type:{
				required: "Please, Select House Type"
			},
			details:{
				minlength:"More than 1 Character Allowed",
				maxlength:"Max 50 Character Allowed",
			}
		}
	});

	$('#meeting_form').validate({
		rules:{
			society_id:{
				required:true
			},
			ondate:{
				required:true
			},
			stime:{
				required:true
			},
			etime:{
				required:true
			},
			subject:{
				required :true,
				minlength:2,
				maxlength:50
			},
			location:{
				required:true,
				minlength:2,
				maxlength:50
			}
		},
		messages:{
			society_id:{
				required:"Please Select A Society"
			},
			ondate:{
				required: "Please Choose Meting Date"
			},
			stime:{
				required:"Please Enter Meeting Start Time"
			},
			etime:{
				required:"Please Enter Meeting End Time"
			},
			subject:{
				required: "Please Enter Subject/ Pupose Of Meeting",
				minlength:"Minimum 2 Characters Allowed",
				maxlength:"Maximum 50 Characters Allowed"
			},
			location:{
				required:"Please Enter Location Of Meeting",
				minlength:"Minimum 2 Characters Allowed",
				maxlength:"Maximum 50 Characters Allowed"
			}
		}
	});
	 jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional(element) || /^[A-Za-z0-9 _-]+$/i.test(value);
}, "");

	$('#login_form').validate({
	   rules:{
	    username:{
	        required:true,
	        // minlength:8,
	        // maxlength:16
	      },
	      password:{
	        required:true,
	        // minlength:8,
	        // maxlength:16
	      }
	    },
	  messages:{
	    username:{
	      required:"Please Enter Username",
	      // minlength:"Minimum 8 Characters Allowed",
	      // maxlength:"Maximum 16 Characters Allowed"
	    },
	    password:{
	      required:"Please Enter Password",
	      // minlength:"Minimum 8 Characters Allowed",
	      // maxlength:"Maximum 16 Characters Allowed"
	     }
	    }
	  });

	$('#sms_form').validate({
	   rules:{
	    society_id:{
	        required:true
	      },
	      reason:{
	        required:true
	      },
	      group:{
	      	required:true
	      },
	      msg:{
	      	required:true
	      }
	    },
	  messages:{
	    society_id:{
	      required:"Please Select Society"
	    },
	    reason:{
	      required:"Please Select Notification Type"
	     },
	     group:{
	     	required:"Please Select Contact Group"
	     },
	     msg:{
	     	required:"Please Enter The Message"
	     }
	    }
	  });
});
